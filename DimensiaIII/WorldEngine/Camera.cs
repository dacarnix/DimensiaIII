﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DimensiaIII.Entities;

namespace DimensiaIII.WorldEngine
{
    public static class Camera
    {

        #region Variables

        private static readonly float zoomUpperLimit = (3f); //Attempt to normalize zooms based off resolution
        private static readonly float zoomLowerLimit = (.4f);

        private static Rectangle _vis;
        private static float _zoom = 1, _rotation;
        private static Matrix _transform;
        private static Vector2 _pos = Vector2.Zero;
        private static int _viewportWidth, _viewportHeight, _worldWidth, _worldHeight;
        private static bool _hasSnapped = false; //set to true after being set to a player
        private static bool _capZoom = true;

        #endregion

        #region Properties

        private static float ZoomRatio
        {
            get { return (float)Math.Sqrt((1920f / Main.MonitorWidth)) ; }
        }

        public static float Zoom
        {
            //We can change the 'base' limits by an offset RELATIVE to a 1080p screen, this way all resolutions will have a 'similar' texture ratio

            get { return ((_zoom) / (ZoomRatio)); }
            set
            {
                _zoom = value * ZoomRatio;
                if (_capZoom)
                {
                    if (_zoom < zoomLowerLimit)
                        _zoom = zoomLowerLimit;
                    if (_zoom > zoomUpperLimit)
                        _zoom = zoomUpperLimit;
                }
                else
                {
                    if (_zoom < 0.005)
                        _zoom = 0.005f;
                    if (_zoom > 20)
                        _zoom = 20;
                }
            }
        }

        public static float ZoomOutterLimit
        {
            get { return zoomLowerLimit / (ZoomRatio); }
        }

        public static float ZoomInnerLimit
        {
            get { return zoomUpperLimit / (ZoomRatio); }
        }

        public static Rectangle VisibleArea
        {
            get
            {
                return  _vis; 
            }
        }
        public static float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        public static Vector2 CentrePos
        {
            get { return _pos + (Main.CentreScreen); }
            set
            {
                TopLeftPos = value - (Main.CentreScreen);
            }

        }

        public static Vector2 TopLeftPos
        {
            get { return _pos; }
            set
            {
                _pos = value;
            }
        }

        public static Vector2 TopLeftPosWithZoom
        {
            get { return _pos - (Main.CentreScreen / Camera.Zoom) + Main.CentreScreen; }
        }


        #endregion

        #region Set up

        public static void Refresh(Viewport viewport, int worldWidth,
           int worldHeight)
        {
            _rotation = 0.0f;
            _viewportWidth = viewport.Width;
            _viewportHeight = viewport.Height;
            _worldWidth = worldWidth;
            _worldHeight = worldHeight;
            _vis = new Rectangle();
        }

        #endregion

        #region Methods

        public static void Update(GameTime gameTime)
        {

            #region Boring Code

            var inverseViewMatrix = Matrix.Invert(_transform);
            var tl = Vector2.Transform(Vector2.Zero, inverseViewMatrix);
            var tr = Vector2.Transform(new Vector2(Main.ScreenWidth, 0), inverseViewMatrix);
            var bl = Vector2.Transform(new Vector2(0, Main.ScreenHeight), inverseViewMatrix);
            var br = Vector2.Transform(new Vector2(Main.ScreenWidth, Main.ScreenHeight), inverseViewMatrix);
            var min = new Vector2(
                MathHelper.Min(tl.X, MathHelper.Min(tr.X, MathHelper.Min(bl.X, br.X))),
                MathHelper.Min(tl.Y, MathHelper.Min(tr.Y, MathHelper.Min(bl.Y, br.Y))));
            var max = new Vector2(
                MathHelper.Max(tl.X, MathHelper.Max(tr.X, MathHelper.Max(bl.X, br.X))),
                MathHelper.Max(tl.Y, MathHelper.Max(tr.Y, MathHelper.Max(bl.Y, br.Y))));
            _vis = new Rectangle((int)min.X, (int)min.Y, (int)(max.X - min.X), (int)(max.Y - min.Y));


            _transform =
           Matrix.CreateTranslation(new Vector3(-CentrePos.X, -CentrePos.Y, 0)) *
           Matrix.CreateRotationZ(Rotation) *
           Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
           Matrix.CreateTranslation(new Vector3(_viewportWidth * 0.5f,
               _viewportHeight * 0.5f, 0));


            #endregion

            #region Movement

            int offsetLimitX = Math.Max(150, Main.ScreenWidth / 6); ; //measured in pixels
            int offsetLimitY = Math.Max(100, Main.ScreenHeight / 5);

            Rectangle innerScreen = new Rectangle(offsetLimitX, offsetLimitY, Main.ScreenWidth - (2*offsetLimitX), Main.ScreenHeight - (2*offsetLimitY));

            if (!Input.MouseCollision(innerScreen) && Input.MouseCollision(Main.ScreenRectangle) && !_hasSnapped)
            {
                float angle = MathAid.FindAngle(Main.CentreScreen, Input.RawMousePosition);
                int firstTmp = Math.Min((int)Input.RawMousePosition.X, (int)Input.RawMousePosition.Y);
                int secTmp = Math.Min((int)(Main.ScreenWidth - Input.RawMousePosition.X), (int)(Main.ScreenHeight - Input.RawMousePosition.Y));
                float minimumVal = Math.Min(firstTmp, secTmp);

                float speed = ((offsetLimitX + offsetLimitY) / 2) / Math.Max(minimumVal, 20); // never reaches a division by zero

                _pos += MathAid.GetVelocity(angle, speed / Zoom);
            }

            if (Input.ScrolledUp)
            {
                Camera.Zoom = (float)Math.Round(Camera.Zoom * 1.2f, 3);
            }
            if (Input.ScrolledDown)
            {
                Camera.Zoom = (float)Math.Round(Camera.Zoom / 1.2f, 3);
            }

            _hasSnapped = false; //reset this

            #endregion


        }

        public static void Move(Vector2 amount)
        {
            _pos += amount;
        }

        public static void LockToEntity(Entity player)
        {
            //this camera simply relies on a vector lock, so really simple.
            CentrePos = player.CentrePosition; //ez
            _hasSnapped = true; //sets it true for one frame
        }

        public static Matrix GetTransformation()
        {
            return _transform;
        }

        public static void SetZoomCap(bool cap)
        {
            _capZoom = cap;
        }

        #endregion

    }
}
