﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using DimensiaIII.WorldEngine.TileEngine;

namespace DimensiaIII.WorldEngine
{

    /// <summary>
    /// A singleton WORLD file,
    /// </summary>
    public static class World
    {

        #region Variables and Properties

        //List of 'maps' incase there are mulitple areas to explore
        //The first [0] element will always be the main world map.
        static List<Map> worldMap;

        public static Map WorldMap
        {
            get {
                return worldMap[0];
            }
        }

        #endregion

        #region Set Up

        public static void LoadGameWorld()
        {
            Map m = new Map(500, 500);
            worldMap = new List<Map>();
            worldMap.Add(m);
            //Purely for testing
        }

        #endregion

        #region XNA Methods

        public static void Update(GameTime gameTime)
        {
            WorldMap.Update(gameTime);
        }


        #region Draw

        public static void DrawBack(SpriteBatch batch)
        {

        }

        public static void DrawMiddle(SpriteBatch batch)
        {
            WorldMap.DrawMiddle(batch);
        }

        public static void DrawFront(SpriteBatch batch)
        {
            WorldMap.DrawFront(batch);
        }

        public static void DrawMostFront(SpriteBatch batch)
        {
            
        }

        public static void DrawAll(SpriteBatch batch)
        {
            DrawBack(batch);
            DrawMiddle(batch);
            DrawFront(batch);
            DrawMostFront(batch);
        }

        #endregion

        #endregion

        #region Methods

        #endregion

    }
}
