﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.WorldEngine.TileEngine
{
    public class TileSet
    {
        //Holds the texture for the tile set and a few variables related
        #region Fields and Properties

        Texture2D image;
        int tilesWide;
        int tilesHigh;
        //OFFSET defines a 'gap' in pixels between each tile on the texture, default to 0
        int offset;
        Rectangle[] sourceRectangles;

        #endregion

        #region Property Region

        public int OffSet
        {
            get { return offset; }
        }

        public Texture2D Texture
        {
            get { return image; }
            private set { image = value; }
        }

        public int TilesWide
        {
            get { return tilesWide; }
            private set { tilesWide = value; }
        }

        public int TilesHigh
        {
            get { return tilesHigh; }
            private set { tilesHigh = value; }
        }

        #endregion

        #region Constructor Region

        public TileSet(Texture2D image,int offSet = 0)
        {
            Texture = image;
            TilesWide = (int)Math.Ceiling(image.Width / (double)(Engine.TileWidth + offset));
            TilesHigh = (int)Math.Ceiling(image.Height/ (double)(Engine.TileHeight + offset));
            this.offset = offSet;

            int tiles = tilesWide * tilesHigh;

            sourceRectangles = new Rectangle[tiles];

            int tile = 0;

            for (int y = 0; y < tilesHigh; y++)
                for (int x = 0; x < tilesWide; x++)
                {
                    sourceRectangles[tile] = new Rectangle(
                        x * Engine.TileWidth + (x * offSet),
                        y * Engine.TileHeight + (y * offSet),
                        Engine.TileWidth,
                        Engine.TileHeight);
                    tile++;
                }
        }

        #endregion

        #region Method Region

        public Rectangle GetRectangle(byte index)
        {
            return sourceRectangles[index];
        }

        #endregion
    }
}
