﻿using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.WorldEngine.TileEngine
{
    /// <summary>
    /// Contains a 'region' of tiles
    /// </summary>
    public class Chunk
    {
        private bool[] _haveTiles;
        private bool[] _layerRendered;
        private RenderTarget2D[] _targets;
        private bool _somethingNotNull;
        private Tile[][,] _tiles;
        private bool _somethingRendered; //this should ALWAYS be true if the chunk is within the screen bounds

        private short _chunkX, _chunkY;

        private byte _potentialElevation; // this is the max height it can GO!!!!
        //this is based of how far away it is from any swamps

        //this will hold things like special entities ie List<TileEntities>

        #region Properties
        
        public bool SomethingNotNull { get { return _somethingNotNull; } }
        public short ChunkXPosition { get { return _chunkX; } }
        public short ChunkYPosition { get { return _chunkY; } }

        public RenderTarget2D[] Targets
        {
            get { return _targets; }
            set { _targets = value; }
        }

        public bool[] HaveTiles
        {
            get { return _haveTiles; }
        }

        public bool SomethingRendered
        {
            get { return _somethingRendered; }
            set { _somethingRendered = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// The CHUNK x and y co-ords
        /// </summary>
        /// <param name="chunkX"></param>
        /// <param name="chunkY"></param>
        public Chunk(short _chunkX, short _chunkY)
        {
            this._chunkX = _chunkX;
            this._chunkY = _chunkY;

            _potentialElevation = 1; //by default

            _haveTiles = new bool[Engine.MapLayers];
            _targets = new RenderTarget2D[Engine.MapLayers];
            _tiles = new Tile[Engine.MapLayers][,];
            _layerRendered = new bool[Engine.MapLayers];

            _somethingNotNull = false;

            for (int i = 0; i < Engine.MapLayers; i++)
            {
                _targets[i] = null;
                _tiles[i] = null;
                _layerRendered[i] = false;
                _haveTiles[i] = false; //Initally all false, this will need to be processed
            }

            _somethingRendered = false;
        }

        #endregion

        #region Setting/Getting tiles

        /// <summary>
        /// X and Y have to be RELATIVE to the TILE inside the CHUNK
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="layer"></param>
        /// <param name="index"></param>
        public void SetTile(int x, int y, int layer, byte index)
        {
            if (_tiles[layer] == null) //tiles will now exist here
            {
                _haveTiles[layer] = true;
                _tiles[layer] = new Tile[Engine.ChunkWidthInTiles, Engine.ChunkHeightInTiles];
            }
            _tiles[layer][x, y].ChangeTile(index);
            _somethingNotNull = true;
        }

        public byte GetTileIndex(int x, int y, int layer)
        {
            return _tiles[layer][x, y].TileIndex;
        }

        /// <summary>
        /// X and Y have to be RELATIVE to the TILE inside the CHUNK
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="layer"></param>
        /// <param name="index"></param>
        public Tile GetTile(int x, int y, int layer)
        {
           return _tiles[layer][x, y];        
        }

        #endregion

        #region Elevation

        /// <summary>
        /// Sets the potetential max elevation
        /// </summary>
        /// <param name="elev"></param>
        public void ElevationMaxSet(byte elev)
        {
            _potentialElevation = elev;
        }

        #endregion

        #region Methods

        public void SetRenderTarget(RenderTarget2D target, int layer)
        {
            _targets[layer] = target;
            _somethingRendered = true;
        }

        public RenderTarget2D GetTarget(int layer)
        {
            return _targets[layer];
        }


        #endregion
    }
}