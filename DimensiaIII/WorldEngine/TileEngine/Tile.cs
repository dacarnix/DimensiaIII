﻿namespace DimensiaIII.WorldEngine.TileEngine
{
    public struct Tile
    {
        public byte TileIndex;

        public Tile(byte index)
        {
            TileIndex = index;
        }

        public void ChangeTile(byte index)
        {
            TileIndex = index;
        }

    }
}
