﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.WorldEngine.TileEngine
{
    public class Map
    {

        #region Field Region

        Chunk[,] tileChunks;

        int mapWidthInTiles, mapWidthInChunks;
        int mapHeightInTiles, mapHeightInChunks;

        Queue<RenderTarget2D> texturePool;
        Vector2 centreChunk; //this for for rendering
        Queue<Point> renderList;
        bool[,] rA;

        int renderTimer = 0;
        int chunkWidthPerScreen, chunkHeightPerScreen;
        //how many chunks will be visible at MAX per screen.
        Point topLeftChunk; //represents what the TOP LEFT chunk is
        Point topLeftDrawChunk; //this one now scales with ZOOM, no need to draw stuff off screen ;)
        int visibleChunksWidth, visibleChunkHeights;

        const int ChunkCullThreshold = 2;
        //Represents how many 'chunks' off the screen you can go before a texture is disposed
        //The larger this is, the less tiles will be culled (in turn, potentially LESS re-processing, at cost of more memory gain)

        public int CHUNKRENDERCOUNT = 0;

        string name;

        #endregion

        #region Property Region

        public Point TopLeftChunk
        {
            get { return topLeftChunk; }
        }

        public Point TopLeftDrawChunk
        {
            get { return topLeftDrawChunk; }
        }

        public int ChunksWidePerScreen
        {
            get { return chunkWidthPerScreen; }
        }

        public int ChunksHighPerScreen
        {
            get { return chunkHeightPerScreen; }
        }

        public int VisibleChunksWidth { get { return visibleChunksWidth; } }
        public int VisibleChunksHigh { get { return visibleChunkHeights; } }

        public int TileWidth
        {
            get { return mapWidthInTiles; }
        }

        public int TileHeight
        {
            get { return mapHeightInTiles; }
        }

        public int ChunkWidth { get { return mapWidthInChunks; } }
        public int ChunkHeight { get { return mapHeightInChunks; } }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int WidthInPixels
        {
            get { return mapWidthInTiles * Engine.TileWidth; }
        }

        public int HeightInPixels
        {
            get { return mapHeightInTiles * Engine.TileHeight; }
        }

        #endregion

        #region Constructor Region

        public Map(int mapWidthInTiles, int mapHeightInTiles)
        {
            renderList = new Queue<Point>();
            texturePool = new Queue<RenderTarget2D>();
            this.mapHeightInTiles = mapHeightInTiles;
            this.mapWidthInTiles = mapWidthInTiles;

            mapWidthInChunks = (int)(mapWidthInTiles / Engine.ChunkWidthInTiles) + 1;
            mapHeightInChunks = (int)(mapHeightInTiles / Engine.ChunkHeightInTiles) + 1;
            rA = new bool[mapWidthInChunks, mapHeightInChunks];

            tileChunks = new Chunk[mapWidthInChunks, mapHeightInChunks];

            for (short x = 0; x < mapWidthInChunks; x++)
            {
                for (short y = 0; y < mapHeightInChunks; y++)
                {
                    rA[x, y] = false;
                    tileChunks[x, y] = new Chunk(x, y);
                }
            }

            topLeftChunk = new Point();
            topLeftDrawChunk = new Point();

            int numberOfRenderTargets = ((Main.MonitorWidth / (Engine.ChunkWidthInTiles * Engine.TileWidth)) * (Main.MonitorHeight / (Engine.ChunkHeightInTiles * Engine.TileHeight))) + 30;
            numberOfRenderTargets *= 3; //for the front layer also.
            for (int i = 0; i < numberOfRenderTargets; i++)
            {
                texturePool.Enqueue(new RenderTarget2D(Pipeline.TextureManager.Device, Engine.ChunkWidthInTiles * Engine.TileWidth, Engine.ChunkHeightInTiles * Engine.TileHeight));
            }
            centreChunk = new Vector2((Engine.TileWidth * Engine.ChunkWidthInTiles) / 2, (Engine.TileHeight * Engine.ChunkHeightInTiles) / 2);
        }

        #endregion

        #region XNA Method Region

        public void Update(GameTime gameTime)
        {
            //For updating chunk positions
            chunkWidthPerScreen = (int)(((Main.MonitorWidth / (Camera.ZoomOutterLimit)) / Engine.ChunkWidthInTiles) / Engine.TileWidth) + 2;
            chunkHeightPerScreen = (int)(((Main.MonitorHeight / (Camera.ZoomOutterLimit)) / Engine.ChunkHeightInTiles) / Engine.TileHeight) + 2;



            topLeftChunk.X = Math.Max((int)((Camera.TopLeftPos.X - (Main.CentreScreenFullResolution.X / Camera.ZoomOutterLimit) + Main.CentreScreenFullResolution.X) / Engine.ChunkWidthInTiles / Engine.TileWidth), 0);
            topLeftChunk.Y = Math.Max((int)((Camera.TopLeftPos.Y - (Main.CentreScreenFullResolution.Y / Camera.ZoomOutterLimit) + Main.CentreScreenFullResolution.Y) / Engine.ChunkHeightInTiles / Engine.TileHeight), 0);

            topLeftDrawChunk.X = Math.Max((int)((Camera.TopLeftPosWithZoom.X) / Engine.ChunkWidthInTiles / Engine.TileWidth), 0);
            topLeftDrawChunk.Y = Math.Max((int)((Camera.TopLeftPosWithZoom.Y) / Engine.ChunkHeightInTiles / Engine.TileHeight), 0);
            //Same as before but adjusts with ZOOM and WIDTH/HEIGHT

            visibleChunksWidth = Math.Min((int)Math.Ceiling((((Main.ScreenWidth / (Camera.Zoom)) / Engine.ChunkWidthInTiles) / Engine.TileWidth)) + 1, chunkWidthPerScreen);
            visibleChunkHeights = Math.Min((int)Math.Ceiling((((Main.ScreenHeight / (Camera.Zoom)) / Engine.ChunkHeightInTiles) / Engine.TileHeight)) + 1, chunkHeightPerScreen);


            #region Chunk Texture culls

            for (int x = topLeftChunk.X - ChunkCullThreshold; x >= topLeftChunk.X - ChunkCullThreshold - 1; x--)
            {

                for (int y = topLeftChunk.Y - ChunkCullThreshold; y <= topLeftChunk.Y + chunkHeightPerScreen + ChunkCullThreshold + 1; y++)
                {
                    if (x >= mapWidthInChunks || y >= mapHeightInChunks || y < 0 || x < 0) continue;
                    if (tileChunks[x, y].SomethingRendered)
                    {
                        DiposeChunk(x, y);
                    }
                }
            }


            for (int y = topLeftChunk.Y - ChunkCullThreshold; y >= topLeftChunk.Y - ChunkCullThreshold - 1; y--)
            {

                for (int x = topLeftChunk.X - ChunkCullThreshold; x <= topLeftChunk.Y + chunkWidthPerScreen + ChunkCullThreshold + 1; x++)
                {
                    if (x >= mapWidthInChunks || y >= mapHeightInChunks || y < 0 || x < 0) continue;
                    if (tileChunks[x, y].SomethingRendered)
                    {
                        DiposeChunk(x, y);
                    }
                }
            }
            //Cull off unneeded chunks

            for (int x = topLeftChunk.X + chunkWidthPerScreen + ChunkCullThreshold; x <= topLeftChunk.X + chunkWidthPerScreen + ChunkCullThreshold + 1; x++)
            {

                for (int y = topLeftChunk.Y - ChunkCullThreshold; y <= topLeftChunk.Y + chunkHeightPerScreen + ChunkCullThreshold + 1; y++)
                {
                    if (x >= mapWidthInChunks || y >= mapHeightInChunks || y < 0 || x < 0) continue;
                    if (tileChunks[x, y].SomethingRendered)
                    {
                        DiposeChunk(x, y);
                    }
                }
            }


            for (int y = topLeftChunk.Y + chunkHeightPerScreen + ChunkCullThreshold; y <= topLeftChunk.Y + chunkHeightPerScreen + ChunkCullThreshold + 1; y++)
            {

                for (int x = topLeftChunk.X - ChunkCullThreshold; x <= topLeftChunk.Y + chunkWidthPerScreen + ChunkCullThreshold + 1; x++)
                {
                    if (x >= mapWidthInChunks || y >= mapHeightInChunks || y < 0 || x < 0) continue;
                    if (tileChunks[x, y].SomethingRendered)
                    {
                        DiposeChunk(x, y);
                    }
                }
            }

            #endregion

        }

        #region Draw

        public void DrawMiddle(SpriteBatch batch)
        {

            Rectangle dest = new Rectangle(0, 0, Engine.ChunkWidthInTiles * Engine.TileWidth, Engine.ChunkHeightInTiles * Engine.TileHeight);
            //Even though the positions in the Update loop show which tiles are to be 'rendered', this is no need as depending on the zoom,
            //only so many need to be drawn.
            for (int y = topLeftDrawChunk.Y; y < topLeftDrawChunk.Y + visibleChunkHeights; y++)
            {
                dest.Y = y * Engine.ChunkHeightInTiles * Engine.TileHeight;
                for (int x = topLeftDrawChunk.X; x < topLeftDrawChunk.X + visibleChunksWidth; x++)
                {
                    dest.X = x * Engine.ChunkWidthInTiles * Engine.TileWidth;
                    if (x >= mapWidthInChunks || y >= mapHeightInChunks || y < 0 || x < 0) continue;
                    for (int a = 0; a < Engine.MapLayers; a++)
                    {
                        float alph = 1f;
                        if (tileChunks[x, y].Targets[a] != null)
                        {
                            batch.Draw(tileChunks[x, y].Targets[a], new Vector2(x * (Engine.ChunkWidthInTiles * Engine.TileWidth), y * (Engine.ChunkHeightInTiles * Engine.TileHeight)), Color.White * alph);
                            //batch.Draw(Pipeline.TextureManager.Pixel, dest, Rand.GetColor() * .2f);
                        }
                    }
                }
            }
        }

        public void DrawFront(SpriteBatch batch)
        {

        }

        public void DrawAll(SpriteBatch batch)
        {
            DrawMiddle(batch);
            DrawFront(batch);
        }

        #endregion

        #region Render Code

        public void CheckRenders(SpriteBatch batch)
        {
            renderTimer++;

            if (renderTimer > Engine.RenderCounter || renderList.Count == 0)
            {
                renderTimer = 0;

                for (int x = topLeftChunk.X; x < topLeftChunk.X + chunkWidthPerScreen; x++)
                {
                    for (int y = topLeftChunk.Y; y < topLeftChunk.Y + chunkHeightPerScreen; y++)
                    {
                        if (x >= mapWidthInChunks || y >= mapHeightInChunks || y < 0 || x < 0) break;
                        if (!tileChunks[x, y].SomethingRendered && !rA[x, y] && tileChunks[x, y].SomethingNotNull)
                        {
                            rA[x, y] = true;
                            renderList.Enqueue(new Point(x, y));
                        }
                    }
                }
            }
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            for (int i = 0; i < Engine.RendersPerFrame; i++)
            {
                if (renderList.Count > 0)
                {
                    Point p = renderList.Dequeue();
                    bool anyError = false;
                    for (int a = 0; a < Engine.MapLayers; a++)
                    {
                        if (tileChunks[p.X, p.Y].HaveTiles[a] && tileChunks[p.X, p.Y].Targets[a] == null)
                        {
                            if (texturePool.Count == 0)
                            {
                                tileChunks[p.X, p.Y].Targets[a] = new RenderTarget2D(Pipeline.TextureManager.Device, Engine.ChunkWidthInTiles * Engine.TileWidth, Engine.ChunkHeightInTiles * Engine.TileHeight);
                            }
                            else
                            {
                                tileChunks[p.X, p.Y].Targets[a] = texturePool.Dequeue();
                            }
                            RenderArea(batch, p.X, p.Y, a); //render the layer
                            if (tileChunks[p.X, p.Y].Targets[a].IsContentLost) //check if target correctly rendered
                            {
                                anyError = true;
                                DiposeChunk(p.X, p.Y);
                            }
                        }
                    }
                    if (!anyError)
                    {
                        CHUNKRENDERCOUNT++;
                        rA[p.X, p.Y] = false;
                    }
                }
            }
            //sw.Stop();
            //System.Diagnostics.Debug.WriteLine(sw.Elapsed.ToString());
        }

        /// <summary>
        /// Clears the chunk!
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void DiposeChunk(int x, int y)
        {
            bool removedTarget = false;
            for (int i = 0; i < Engine.MapLayers; i++)
            {
                if (tileChunks[x, y].Targets[i] != null)
                {
                    texturePool.Enqueue(tileChunks[x, y].Targets[i]);
                    tileChunks[x, y].Targets[i] = null;
                    removedTarget = true;
                }
            }

            if (removedTarget) CHUNKRENDERCOUNT--;
            tileChunks[x, y].SomethingRendered = false;
        }

        private void RenderArea(SpriteBatch batch, int x, int y, int layer)
        {
            try
            {
                Pipeline.TextureManager.Device.SetRenderTarget(tileChunks[x, y].Targets[layer]);
                Pipeline.TextureManager.Device.Clear(Color.Transparent);
                batch.Begin();
                Rectangle dest = new Rectangle(0, 0, Engine.TileWidth, Engine.TileHeight);
                for (int X = 0; X < Engine.ChunkWidthInTiles; X++)
                {
                    dest.X = X * Engine.TileWidth;
                    for (int Y = 0; Y < Engine.ChunkHeightInTiles; Y++)
                    {
                        dest.Y = Y * Engine.TileHeight;

                        batch.Draw(Engine.TileSets[0].Texture, dest, Engine.TileSets[0].GetRectangle(tileChunks[x, y].GetTileIndex(X, Y, layer)), Color.White);

                    }
                }

                tileChunks[x, y].SomethingRendered = true;
            }
            catch
            {
                tileChunks[x, y].SomethingRendered = false;
            }
            finally
            {
                batch.End();
                Pipeline.TextureManager.Device.SetRenderTarget(null);
            }
        }

        #endregion

        #region Force Render

        /// <summary>
        /// Force re-renders a chunk, ie if a tile had changed on it, etc.
        /// Takesin a position, which will be converted to chunk co-ords, then force re-render
        /// </summary>
        /// <param name="position"></param>
        public void ForceRender(Vector2 position)
        {
            Point chunk = Engine.VectorToChunk(position);
            if (tileChunks[chunk.X, chunk.Y].SomethingRendered)
            {
                DiposeChunk(chunk.X, chunk.Y);
                renderList.Enqueue(chunk); //re-render this!
            }
        }

        #endregion

        #endregion

        #region Getting/Setting Tiles Types

        public byte GetTileIndexFromVectorPosition(Vector2 position, int layer)
        {
            return GetTileIndexFromTilePosition(Engine.VectorToCell(position), layer);
        }

        public byte GetTileIndexFromTilePosition(Point position, int layer)
        {
            return GetTileIndexFromTilePosition(position.X, position.Y, layer);
        }

        public byte GetTileIndexFromTilePosition(int x, int y, int layer)
        {
            int chunkX = (int)x / Engine.ChunkWidthInTiles;
            int chunkY = (int)y / Engine.ChunkHeightInTiles;

            //the specific tile IN the chunk
            int specificChunkX = (int)x % Engine.ChunkWidthInTiles;
            int specificChunkY = (int)y % Engine.ChunkHeightInTiles;
            int i;

            return tileChunks[chunkX, chunkY].GetTileIndex(specificChunkX, specificChunkY, layer);
        }


        public void SetTileIndexFromVectorPosition(Vector2 position, int layer, byte index)
        {
            SetTileIndexFromTilePosition(Engine.VectorToCell(position), layer, index);
        }

        public void SetTileIndexFromTilePosition(Point position, int layer, byte index)
        {
            SetTileIndexFromTilePosition(position.X, position.Y, layer, index);
        }

        public void SetTileIndexFromTilePosition(int x, int y, int layer, byte index)
        {
            int chunkX = (int)x / Engine.ChunkWidthInTiles;
            int chunkY = (int)y / Engine.ChunkHeightInTiles;
            //the specific chunk

            //the specific tile IN the chunk
            int specificChunkX = (int)x % (Engine.ChunkWidthInTiles);
            int specificChunkY = (int)y % (Engine.ChunkHeightInTiles);

            tileChunks[chunkX, chunkY].SetTile(specificChunkX, specificChunkY, layer, index);
        }


        #endregion

    }
}
