﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.WorldEngine.TileEngine
{
    public static class Engine
    {
        //This class is pretty basic, just holds the 'constants' for the tile width/height
        //This will hold all the textures for all the tilesets

        #region Field Region

        static int tileWidth;
        static int tileHeight;
        static List<TileSet> tileSets;
        static Vector2 centreTile;
        static bool[] transparentTiles; //0-255, flag 1 if theyre transparanent.
        public const byte EmptyTile = 255;
        static int chunkWidthPixels, chunkHeightPixels;

        private static byte[] _tileFrictions; //each unique tile index is linked with a friction.

        //Chunk related variables
        public const int ChunkWidthInTiles = 24, ChunkHeightInTiles = 24;
        public const int RendersPerFrame = 2; //how many chunks to be rendered per frame
        public const byte MapLayers = 4; //How many LAYERS (ie distinct heights) does a map have?
        public const int RenderCounter = 20; //How many frames should new renders be checked?

        #endregion

        #region Property Region

        public static byte[] TileFrictions
        {
            get { return _tileFrictions; }
        }

        public static int TileWidth
        {
            get { return tileWidth; }
            set { tileWidth = value; }
        }

        public static int TileHeight
        {
            get { return tileHeight; }
            set { tileHeight = value; }
        }

        public static List<TileSet> TileSets
        {
            get { return tileSets; }
        }

        public static Vector2 TileCentre
        {
            get { return centreTile; }
        }

        public static int ChunkHeightPixels { get { return chunkHeightPixels; } }

        public static int ChunkWidthPixels { get { return chunkWidthPixels; } }

        #endregion

        #region Methods

        public static void Initilize(int tileWidth, int tileHeight)
        {
            Engine.tileWidth = tileWidth;
            Engine.tileHeight = tileHeight;
            centreTile = new Vector2(tileWidth / 2, tileHeight / 2);
            tileSets = new List<TileSet>();
            transparentTiles = new bool[255];
            _tileFrictions = new byte[255];
            for (int i = 0; i < transparentTiles.Length; i++)
            {
                transparentTiles[i] = false;
                _tileFrictions[i] = 0;
            }

            //set the TRANSPARENT FLAGS HERE
            transparentTiles[1] = true;

            chunkHeightPixels = Engine.TileHeight * ChunkHeightInTiles;
            chunkWidthPixels = Engine.TileWidth * ChunkWidthInTiles;
        }

        public static Point VectorToCell(Vector2 position)
        {
            return new Point((int)position.X / tileWidth, (int)position.Y / tileHeight);
        }

        public static void LoadTileSets(ContentManager content)
        {
            TileSet ts = new TileSet(content.Load<Texture2D>("Test\\testSet"));
            for (int i = 0; i < 10; i++)
            { //add 10 times for testing
                tileSets.Add(ts);
            }
        }

        public static bool IsTransparentTile(byte b)
        {
            return transparentTiles[b];
        }

        /// <summary>
        /// Converts a vector to a chunk position
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Point VectorToChunk(Vector2 point)
        {
            return new Point((int)(point.X / (Engine.ChunkWidthInTiles * Engine.TileWidth)), (int)(point.Y / (Engine.ChunkHeightInTiles * Engine.TileHeight)));
        }


        #endregion

    }
}
