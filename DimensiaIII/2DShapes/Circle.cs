﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DimensiaIII.WorldEngine;

namespace DimensiaIII._2DShapes
{

    //Code found online, and should only be used for testing
    //Is expensive to use, and thus is not intended for production
    //(use actual circle textures instead)

    public class Circle : IDisposable
    {

        #region Variables and Properties

        private int CalculatePointCount()
        {
            return (int)Math.Ceiling(Math.Sqrt(Radius) / 10) + 25;
        }

        private GraphicsDeviceManager graphics;
        private VertexPositionColor[] vertices;
        private static BasicEffect effect;

        private float x;
        public float X
        {
            get { return x; }
            set { x = value; InitializeVertices(); }
        }
        private float y;
        public float Y
        {
            get { return y; }
            set { y = value; InitializeVertices(); }
        }
        private float radius;
        public float Radius
        {
            get { return radius; }
            set { radius = (value < 1) ? 1 : value; InitializeVertices(); }
        }
        private Color color;
        public Color Color
        {
            get { return color; }
            set { color = value; InitializeVertices(); }
        }
        public int Points
        {
            get { return CalculatePointCount(); }
        }

        #endregion

        #region Constructor

        public void Regen(Vector2 pos, int rad, Color c, GraphicsDeviceManager g)
        {
            this.x = pos.X;
            this.y = pos.Y;
            this.radius = rad;
            this.color = c;
            this.graphics = g;
            Initialize();
        }

        #endregion

        #region XNA Methods

        public void Draw()
        {
            if (effect == null || graphics == null || vertices == null) return;

            effect.CurrentTechnique.Passes[0].Apply();

            effect.World = Camera.GetTransformation();
            effect.CurrentTechnique.Passes[0].Apply();

            graphics.GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineStrip, vertices, 0, vertices.Length - 1);

        }

        #endregion

        #region Boring code

        public void Dispose()
        {
            //effect.Dispose();
        }

        private void Initialize()
        {
            InitializeBasicEffect();
            InitializeVertices();
        }

        private void InitializeBasicEffect()
        {
            if (effect == null)
            {
                effect = new BasicEffect(graphics.GraphicsDevice);
                effect.VertexColorEnabled = true;
            }
            effect.Projection = Matrix.CreateOrthographicOffCenter
                (0, graphics.GraphicsDevice.Viewport.Width,
                 graphics.GraphicsDevice.Viewport.Height, 0,
                 0, 1);
        }

        private void InitializeVertices()
        {
            if (CalculatePointCount() < 2) return;
            vertices = new VertexPositionColor[CalculatePointCount()];
            if (vertices.Length < 2) return;
            var pointTheta = ((float)Math.PI * 2) / (vertices.Length - 1);
            for (int i = 0; i < vertices.Length; i++)
            {
                var theta = pointTheta * i;
                var x = X + ((float)Math.Sin(theta) * Radius);
                var y = Y + ((float)Math.Cos(theta) * Radius);
                vertices[i].Position = new Vector3(x, y, 0);
                vertices[i].Color = Color;
            }
            vertices[vertices.Length - 1] = vertices[0];
        }

        #endregion

    }
}
