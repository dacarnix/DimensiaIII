﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using DimensiaIII.Pipeline;

using DimensiaIII._2DShapes;
using DimensiaIII.WorldEngine;

namespace DimensiaIII
{

    //This class is incredibly hacky
    //Only intended for testing purposes, plz.

    public static class Shapes2D
    {

        #region Variables and Properties

        private static Queue<Circle> _drawCircles;

        private static Main _game;

        public static int ShapeCount { get { return _drawCircles.Count; } }

        #endregion

        #region Set Up

        public static void SetUp(Game g)
        {

            _game = (Main)g;

            //similar to how the particle engine works, have a list of free
            _drawCircles = new Queue<Circle>();

            for (int i = 0; i < 1000; i++)
            {
                _drawCircles.Enqueue(new Circle());
            }

        }

        #endregion

        #region Methods (Adding shapes etc)

        #region Rectangle

        public static void DrawRectangle(SpriteBatch spriteBatch, Rectangle rectangleToDraw)
        {
            DrawRectangle(spriteBatch, rectangleToDraw, 1, Color.White);
        }

        public static void DrawRectangle(SpriteBatch spriteBatch, Rectangle rectangleToDraw, int thicknessOfBorder)
        {
            DrawRectangle(spriteBatch, rectangleToDraw, thicknessOfBorder, Rand.GetColor());
        }

        public static void DrawRectangle(SpriteBatch spriteBatch, Rectangle rectangleToDraw, int thicknessOfBorder, Color borderColor)
        {

            // Draw top line
            spriteBatch.Draw(TextureManager.Pixel, new Rectangle(rectangleToDraw.X, rectangleToDraw.Y, rectangleToDraw.Width, thicknessOfBorder), borderColor);

            // Draw left line
            spriteBatch.Draw(TextureManager.Pixel, new Rectangle(rectangleToDraw.X, rectangleToDraw.Y, thicknessOfBorder, rectangleToDraw.Height), borderColor);

            // Draw right line
            spriteBatch.Draw(TextureManager.Pixel, new Rectangle((rectangleToDraw.X + rectangleToDraw.Width - thicknessOfBorder),
                                            rectangleToDraw.Y,
                                            thicknessOfBorder,
                                            rectangleToDraw.Height), borderColor);
            // Draw bottom line
            spriteBatch.Draw(TextureManager.Pixel, new Rectangle(rectangleToDraw.X,
                                            rectangleToDraw.Y + rectangleToDraw.Height - thicknessOfBorder,
                                            rectangleToDraw.Width,
                                            thicknessOfBorder), borderColor);
        }


        public static void DrawFilledRectangle(SpriteBatch batch, Rectangle toDraw, Color fillColor, Color borderColor, float alpha,  int thickshakeOfBorder = 4)
        {
            batch.Draw(Pipeline.TextureManager.Pixel, toDraw, fillColor * alpha);
            DrawRectangle(batch, toDraw, thickshakeOfBorder, borderColor);
        }

        #endregion

        #region Lines

        public static void DrawLine(SpriteBatch spriteBatch, Vector2 point1, Vector2 point2)
        {
            DrawLine(spriteBatch, point1, point2, Color.White);
        }

        public static void DrawLine(SpriteBatch spriteBatch, Vector2 point1, Vector2 point2, Color color)
        {
            DrawLine(spriteBatch, point1, point2, Color.White, (int)(2 / (Camera.Zoom * 2)));
        }

        public static void DrawLine(SpriteBatch spriteBatch, Vector2 point1, Vector2 point2, Color color, int lineWidth)
        {
            float angle = MathAid.FindAngle(point1, point2);
            float length = Vector2.Distance(point1, point2);
            spriteBatch.Draw(TextureManager.Pixel, point1, null, color,
            angle, Vector2.Zero, new Vector2(length, lineWidth),
            SpriteEffects.None, 0f);
        }

        #endregion

        #region Circles

        public static void DrawCircle(Vector2 position, int radius)
        {
            DrawCircle(position, radius, Color.White);
        }

        public static void DrawCircle(Vector2 position, int radius, Color c)
        {
            Circle circ = _drawCircles.Dequeue();          
            circ.Regen(position, radius, c, _game.GraphicsManager);
            circ.Draw();
            circ.Dispose();
            _drawCircles.Enqueue(circ); //ez
        }

        #endregion

        #region Rectangles



        #endregion

        #endregion

    }

}
