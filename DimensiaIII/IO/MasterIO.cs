﻿using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;

using Microsoft.Xna.Framework.Input;

namespace DimensiaIII.IO
{
    public static class MasterIO
    {

        #region Variables and Properties

        private static Settings _settings = null;

        private const string FileExtension = ".data";
        public static string BasePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + "My Games" + Path.DirectorySeparatorChar + Main.GameName + Path.DirectorySeparatorChar;
        public static string PlayerPath = BasePath + Path.DirectorySeparatorChar + "Players" + Path.DirectorySeparatorChar;
        public static string WorldPath = BasePath + Path.DirectorySeparatorChar + "Maps" + Path.DirectorySeparatorChar;
        public static string SettingsPath = BasePath + "settings" + FileExtension;
        public static string ScreenshotPath = BasePath + Path.DirectorySeparatorChar + "Screenshots" + Path.DirectorySeparatorChar;

        //Fancy code for encryption purposes
        private static MD5CryptoServiceProvider _md5;
        private static UTF8Encoding _utf;
        private static TripleDESCryptoServiceProvider _des;
        private static ICryptoTransform _tras;
        private const string Key = "L337chy.";
        private static Keys _screenshotKey;
        private const Keys DefaultScreenshotKey = Keys.F3;

        public static Keys ScreenshotKey { get { return _screenshotKey; } }

        #endregion

        #region Initialization

        public static void SetUp()
        {
            _settings = new Settings();

            _screenshotKey = DefaultScreenshotKey;

            if (!Directory.Exists(PlayerPath))
            {
                Directory.CreateDirectory(PlayerPath);
            }

            if (!Directory.Exists(WorldPath))
            {
                Directory.CreateDirectory(WorldPath);
            }

            if (!Directory.Exists(ScreenshotPath))
            {
                Directory.CreateDirectory(ScreenshotPath);
            }

            _md5 = new MD5CryptoServiceProvider();
            _utf = new UTF8Encoding();
            _des = new TripleDESCryptoServiceProvider();

            _des.Key = _md5.ComputeHash(_utf.GetBytes(Key));
            _des.Mode = CipherMode.ECB;
            _des.Padding = PaddingMode.PKCS7;
            _tras = _des.CreateEncryptor();

        }

        #endregion

        #region Methods

        public static bool HasPlayerControls()
        {
            return (File.Exists(PlayerPath + "controls" + FileExtension));
        }

        public static Keys[] LoadControls()
        {
            return null;
        }

        public static void LoadSettings()
        {

        }

        public static string[] LoadNetworking()
        {
            return null;
        }

        public static void SaveAll()
        {
            SaveSettings();
        }

        private static void SaveSettings()
        {

        }

        #endregion

        #region Encyrption Methods



        #endregion

        #region ScreenShot code

        


        #endregion

    }
}
