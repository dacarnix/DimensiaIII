﻿using System;
using System.IO;
using DimensiaIII.WorldEngine.TileEngine;

namespace DimensiaIII.IO
{
    /// <summary>
    /// Class used to load in maps/save map data.
    /// </summary>
    public class MapIO
    {

        //Consider using shorts/bytes over ints, and this will heavily save on file size.
        //Ensure to use a binary writer

        /// <summary>
        /// If path is null, that means that we will save it to a default directory
        /// </summary>
        /// <param name="map"></param>
        /// <param name="path"></param>
        public void SaveMap(Map map, String path = null)
        {

            if (path == null) //set it to the default directory
            {
                path = MasterIO.WorldPath + map.Name;
            }

            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                using (BinaryWriter writer = new BinaryWriter(fs))
                {

                }
            }
        }


        public Map LoadMap(String path)
        {
            return null;
        }

    }
}
