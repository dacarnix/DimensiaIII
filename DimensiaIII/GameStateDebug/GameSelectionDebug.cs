﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using DimensiaIII.Effects;
using DimensiaIII.Effects.Particles;

namespace DimensiaIII.Debug
{
    /// <summary>
    /// Extended for menu screen debugging. 
    /// Currently, this is mainly for particle debugs
    /// </summary>
    public class GameSelectionDebug : BaseDebug
    {   
        
        private Keys _explosionParticle, _andKey;

        public GameSelectionDebug()
        {
            _andKey = Keys.LeftShift;
            _explosionParticle = Keys.D1;
        }

        protected override void UpdateDevelopment(GameTime gameTime)
        {
            EffectManager.Update(gameTime);
            if (Input.KeyDown(_andKey))
            {
                if (Input.KeyPressed(_explosionParticle))
                {
                    EffectManager.AddParticles(ParticleType.Explosion, Input.RawMousePosition, 100, Rand.GetColor());
                }
                EffectManager.AddParticles(ParticleType.MouseFollow, Input.RawMousePosition, 10,Rand.GetColor());
            }

            base.UpdateDevelopment(gameTime);
        }

        protected override void DrawDevelopment(GameTime gameTime)
        {
            EffectManager.DrawAll(SpriteBatch);
            base.DrawDevelopment(gameTime);
        }

    }
}
