﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Diagnostics;
using DimensiaIII.Effects;
using DimensiaIII.Networking;
using DimensiaIII.Pipeline;
using DimensiaIII.WorldEngine;

namespace DimensiaIII.Debug
{
    enum DebugPage
    {
        General = 385,
        TileEngine = 500,
        Efficiency = 250,
        Networking = 120
    }

    /// <summary>
    /// A Debug class, which is anchored to an individual play screen
    /// Follows the TEMPLATE method (Update AND draw actually are template methods..)
    /// </summary>
    public class BaseDebug
    {
        #region Variables

        private Keys _toggleDebug, _cycleDebugScreen;

        private Main _game;

        private DebugPage _info;

        private float _fps, _updateInterval = 1.0f, _timeSinceLastUpdate = 0.0f, _frameCount = 0;

        private int _averageFPS = 99, _counted = 1, _lowestFPS = 60, _totalFPScount, _lowParticle = 0;

        private bool _screenshotSuccess;

        private float _screenshotTimer, _screenshotAlpha;
        private const float _screenshotBaseTimer = 1500;

        protected SpriteBatch SpriteBatch { get { return _game.SpriteBatch; } }

        #endregion

        #region Constructor
        public BaseDebug()
        {
            _screenshotAlpha = 0;
            _screenshotTimer = 0;
            _screenshotSuccess = false;
            _info = DebugPage.General;
            _toggleDebug = Keys.F1;
            _cycleDebugScreen = Keys.OemTilde;

        }

        public void SetGame(Game game)
        {
            _game = (Main)game;
        }

        #endregion

        #region Template Methods

        /// <summary>
        /// Update is the template method
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            if (Input.KeyPressed(_toggleDebug))
            {
                Main.DEBUG = !Main.DEBUG;
            }
            UpdateAlways(gameTime);
            if (Main.DEBUG)
            {
                UpdateDebug(gameTime);
            }
            if (Main.DEVELOPMENT)
            {
                UpdateDevelopment(gameTime);
            }
        }

        public void Draw(GameTime gameTime)
        {
            SpriteBatch.Begin();
            DrawDebug(gameTime);
            DrawDevelopment(gameTime);
            SpriteBatch.End();
        }

        #endregion

        #region Update Template Methods


        /// <summary>
        /// Always called DEBUG information
        /// </summary>
        /// <param name="gameTime"></param>
        protected virtual void UpdateAlways(GameTime gameTime)
        {
            if (_screenshotTimer > 0)
            {
                _screenshotTimer -= ((float)gameTime.ElapsedGameTime.TotalMilliseconds);
                _screenshotAlpha = Math.Min(1, (_screenshotTimer / _screenshotBaseTimer) + 0.2f);
            }
        }

        /// <summary>
        /// This is only called IF debug is on
        /// </summary>
        /// <param name="gameTime"></param>
        protected virtual void UpdateDebug(GameTime gameTime)
        {
            if (Input.KeyPressed(_cycleDebugScreen))
            {
                if (_info == DebugPage.General) _info = DebugPage.TileEngine;
                else if (_info == DebugPage.TileEngine) _info = DebugPage.Efficiency;
                else if (_info == DebugPage.Efficiency) _info = DebugPage.Networking;
                else _info = DebugPage.General; //Cycle through the options
            }
        }

        /// <summary>
        /// This is only called IF you're developing 
        /// </summary>
        /// <param name="gameTime"></param>
        protected virtual void UpdateDevelopment(GameTime gameTime)
        {

        }



        #endregion

        #region Draw Template Methods

        //This is called in the DRAW
        private void FPSUpdate(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            _frameCount++;
            _timeSinceLastUpdate += elapsed;

            if (_timeSinceLastUpdate > _updateInterval)
            {
                _fps = (float)Math.Round(_frameCount / _timeSinceLastUpdate);

                _frameCount = 0;
                _timeSinceLastUpdate -= _updateInterval;

            }

            if (gameTime.TotalGameTime.TotalSeconds > 2)
            {
                _counted++;
                _totalFPScount += (int)_fps;
                if (_lowestFPS != 0 && _fps != 0)
                {
                    if (_fps < _lowestFPS)
                    {
                        _lowestFPS = (int)_fps;
                        _lowParticle = Effects.EffectManager.ParticleCount;
                    }
                }

            }

            _averageFPS = _totalFPScount / _counted;
        }

        protected virtual void DrawDebug(GameTime gameTime)
        {
            FPSUpdate(gameTime); //for FPS, the updates should be done in the DRAW method

            if (Main.DEBUG)
            {

                Shapes2D.DrawFilledRectangle(SpriteBatch, new Rectangle(0, 0, 500, (int)_info), Color.Black, Color.Gray, 0.4f);

                Vector2 drawTo = new Vector2(10, 5);
                SpriteBatch.DrawString(FontManager.Clean, "Toggle Debug with the ~ key", drawTo, Color.Yellow);
                drawTo.Y += 20;
                SpriteBatch.DrawString(FontManager.Clean, "Ctrl + Tab to change Debug Info", drawTo, Color.Yellow);
                drawTo.Y += 30;
                SpriteBatch.DrawString(FontManager.Clean, "Debug Info: " + _info.ToString(), drawTo, Color.Red);
                drawTo.Y += 40;

                if (_info == DebugPage.General)
                {
                    SpriteBatch.DrawString(FontManager.Clean, "Current State: " + _game.StateManager.CurrentState.ToString().Split('.')[2], drawTo, Color.White);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Camera Top Left: X - " + ((int)Math.Round(Camera.TopLeftPos.X)).ToString() + ", Y - " + ((int)Math.Round(Camera.TopLeftPos.Y)).ToString(), drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Camera Top Left (Zoom): X - " + ((int)Math.Round(Camera.TopLeftPosWithZoom.X)).ToString() + ", Y - " + ((int)Math.Round(Camera.TopLeftPosWithZoom.Y)).ToString(), drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Camera Zoom: " + Camera.Zoom.ToString(), drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Camera Rotation: " + Camera.Rotation.ToString(), drawTo, Color.White);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Monitor Width: " + Main.MonitorWidth, drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Monitor Height: " + Main.MonitorHeight, drawTo, Color.White);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Mouse Position: X - " + ((int)Math.Round(Input.MousePosition.X)).ToString() + ", Y - " + ((int)Math.Round(Input.MousePosition.Y)).ToString(), drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Mouse Screen Position: X - " + ((int)Math.Round(Input.RawMousePosition.X)).ToString() + ", Y - " + ((int)Math.Round(Input.RawMousePosition.Y)).ToString(), drawTo, Color.White);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Particle Count: " + EffectManager.ParticleCount.ToString(), drawTo, Color.White);
                    drawTo.Y += 40;
                }
                else if (_info == DebugPage.TileEngine)
                {
                    SpriteBatch.DrawString(FontManager.Clean, "Map Width (In Tiles): " + World.WorldMap.TileWidth.ToString(), drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Map Height (In Tiles) " + World.WorldMap.TileHeight.ToString(), drawTo, Color.White);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Chunk Rendered Count: " + World.WorldMap.CHUNKRENDERCOUNT.ToString(), drawTo, Color.Pink);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Visible Chunks in X: " + World.WorldMap.VisibleChunksWidth.ToString(), drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Visible Chunks in Y: " + World.WorldMap.VisibleChunksHigh.ToString(), drawTo, Color.White);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Chunks Per Screen At Max Wide: " + World.WorldMap.ChunksWidePerScreen.ToString(), drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Chunks Per Screen At Max High: " + World.WorldMap.ChunksHighPerScreen.ToString(), drawTo, Color.White);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Top Left Chunk: " + World.WorldMap.TopLeftChunk.ToString(), drawTo, Color.White);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Top Left Chunk to Draw: " + World.WorldMap.TopLeftDrawChunk.ToString(), drawTo, Color.White);
                    drawTo.Y += 40;
                    SpriteBatch.DrawString(FontManager.Clean, "Current Chunk: " + WorldEngine.TileEngine.Engine.VectorToChunk(Input.MousePosition), drawTo, Color.White);
                    drawTo.Y += 40;
                }
                else if (_info == DebugPage.Efficiency)
                {
                    SpriteBatch.DrawString(FontManager.Clean, "FPS: " + _fps.ToString(), drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Av FPS: " + _averageFPS.ToString(), drawTo, Color.White);
                    drawTo.Y += 20;
                    SpriteBatch.DrawString(FontManager.Clean, "Min FPS: " + _lowestFPS.ToString() + " with P-Count of " + _lowParticle.ToString(), drawTo, Color.White);
                    drawTo.Y += 30;
                    SpriteBatch.DrawString(FontManager.Clean, "Average Draw Cycle Time: " + Math.Round(DimensiaIII.GameStates.GamePlayScreen.AverageSecondDrawCycle, 2).ToString() + "ms", drawTo, Color.White);
                    drawTo.Y += 30;
                    SpriteBatch.DrawString(FontManager.Clean, "Average Update Cycle Time: " + Math.Round(DimensiaIII.GameStates.GamePlayScreen.AverageSecondUpdateCycle, 2).ToString() + "ms", drawTo, Color.White);
                    drawTo.Y += 30;
                    SpriteBatch.DrawString(FontManager.Clean, "Current Memory Usage: " + Math.Round((Process.GetCurrentProcess().PrivateMemorySize64) / (double)1000000, 2).ToString() + "MB", drawTo, Color.White);
                    drawTo.Y += 40;
                }
                else if (_info == DebugPage.Networking)
                {
                    SpriteBatch.DrawString(FontManager.Clean, "Active Connections: " + NetworkGame.PeopleConnected.ToString(), drawTo, Color.White);
                    drawTo.Y += 40;
                }

                if (_screenshotTimer > 0)
                {
                    string message = (_screenshotSuccess) ? "Screenshot succesfully taken" : "Screenshot failed";
                    Color c = (_screenshotSuccess) ? Color.LimeGreen : Color.Red;
                    SpriteBatch.DrawString(FontManager.Clean, message, drawTo, c * _screenshotAlpha);
                }
            }
        }

        protected virtual void DrawDevelopment(GameTime gameTime)
        {

        }

        #endregion

        #region Other

        public void PulseScreenshot(bool success)
        {
            _screenshotSuccess = success;
            _screenshotTimer = _screenshotBaseTimer;
        }

        #endregion

    }
}
