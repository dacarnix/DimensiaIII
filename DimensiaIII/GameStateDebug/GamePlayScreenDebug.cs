﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DimensiaIII.Debug
{
    /// <summary>
    /// Extended, so as a developer, we are able to fully refresh the world generation
    /// Or individually, refresh only the elevations, OR biomes, OR rivers.
    /// </summary>
    public class GamePlayScreenDebug : BaseDebug
    {
        private Keys _refreshMap, _toggleVoronoi, _toggleElevation, _toggleBiome, _toggleRiver, _andKey;
        private Keys _toggleWholeRender;
        //to render ANY overlay of cells

        public GamePlayScreenDebug() :base()
        {
            _andKey = Keys.LeftShift;
            _refreshMap = Keys.D1;
            _toggleVoronoi = Keys.D2;
            _toggleElevation = Keys.D3;
            _toggleBiome = Keys.D4;
            _toggleRiver = Keys.D5;
            _toggleWholeRender = Keys.D6;
        }

        protected override void UpdateDevelopment(GameTime gameTime)
        {

            base.UpdateDevelopment(gameTime);
        }

        protected override void DrawDevelopment(GameTime gameTime)
        {
            base.DrawDevelopment(gameTime);
        }

    }
}
