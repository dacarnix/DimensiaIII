﻿using System;

using Microsoft.Xna.Framework;

namespace DimensiaIII
{
    public static class MathAid
    {

        #region Methods

        /// Turn a Vector2 into a rectangle
        /// </summary>
        /// <param name="position">The X and Y of the new rectangle</param>
        /// <param name="width">the width</param>
        /// <param name="height">the height</param>
        /// <returns>The new rectangle</returns>
        public static Rectangle Vector2ToRectangle(Vector2 position, int width, int height)
        {
            return new Rectangle((int)position.X, (int)position.Y, width, height);

        }

        /// <summary>
        /// Turn two Vector2's into a rectangle
        /// </summary>
        /// <param name="position">The X and Y of the new rectangle</param>
        /// <param name="size">the new size with x being the width and y the height</param>
        /// <returns></returns>

        public static Rectangle Vector2ToRectangle(Vector2 position, Vector2 size)
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }

        public static double RoundtoSF(double d, int digits)
        {
            if (d == 0)
            {
                return 0;
            }

            double scale = Math.Pow(10, Math.Floor(Math.Log10(Math.Abs(d))) + 1);
            return scale * Math.Round(d / scale, digits);
        }

        public static double TruncatetoSF(double d, int digits)
        {
            if (d == 0)
            {
                return 0;
            }

            double scale = Math.Pow(10, Math.Floor(Math.Log10(Math.Abs(d))) + 1 - digits);
            return scale * Math.Round(d / scale);
        }

        public static Vector2 GetVelocity(float angle, float speed)
        {
            Vector2 velocity;
            velocity.X = speed * ((float)Math.Cos(angle));
            velocity.Y = speed * ((float)Math.Sin(angle));
            return velocity;
        }

        public static Vector2 GetVelocity(float angle, Vector2 speed)
        {
            Vector2 velocity;
            velocity.X = speed.X * ((float)Math.Cos(angle));
            velocity.Y = speed.Y * ((float)Math.Sin(angle));
            return velocity;
        }


        public static Vector2 LineIntersectionPoint(Point ps1, Point pe1, Point ps2,
             Point pe2)
        {
            // Get A,B,C of first line - points : ps1 to pe1
            float A1 = pe1.Y - ps1.Y;
            float B1 = ps1.X - pe1.X;
            float C1 = A1 * ps1.X + B1 * ps1.Y;

            // Get A,B,C of second line - points : ps2 to pe2
            float A2 = pe2.Y - ps2.Y;
            float B2 = ps2.X - pe2.X;
            float C2 = A2 * ps2.X + B2 * ps2.Y;

            // Get delta and check if the lines are parallel
            float delta = A1 * B2 - A2 * B1;
            if (delta == 0)
                return Vector2.Zero;

            // now return the Vector2 intersection point
            return new Vector2(
                (B2 * C1 - B1 * C2) / delta,
                (A1 * C2 - A2 * C1) / delta
            );
        }

        public static float FindAngle(Vector2 position, Vector2 targetPosition)
        {
            Vector2 place = new Vector2(targetPosition.X - position.X, targetPosition.Y - position.Y);
            float rotation = (float)Math.Atan2(place.Y, place.X);
            return rotation;

        }

        /// <summary>
        /// Find the rotation from one scaled position to a non scaled position
        /// for instance a scaled object interacting with a mouse.
        /// </summary>
        /// <param name="position">The current position</param>
        /// <param name="targetPosition">The position to find the rotation to</param>
        /// <param name="scale">the scale of the position object</param>
        /// <returns>Returns the rotation to the designated nonscaled position</returns>

        public static float FindAngle(Vector2 position, Vector2 targetPosition, float scale)
        {
            Vector2 place = new Vector2(targetPosition.X - position.X * scale, targetPosition.Y - position.Y * scale);
            float rotation = (float)Math.Atan2(place.Y, place.X);
            return rotation;
        }

        #endregion

        /*
        Gets the angle between 3 points (make b the centre common point)
        RETURNS IN DEGREES
        */
        public static double GetInternalAngle(Vector2 a, Vector2 b, Vector2 c, bool radians = false)
        {
            double v1x = c.X - b.X;
            double v1y = c.Y - b.Y;
            double v2x = a.X - b.X;
            double v2y = a.Y - b.Y;

            double angle = Math.Atan2(v1x, v1y) - Math.Atan2(v2x, v2y);
            if (!radians)
            {
                angle = MathHelper.ToDegrees((float)angle);
            }
            return angle;
        }

    }
}
