﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using DimensiaIII.WorldEngine;

namespace DimensiaIII
{
    public class Input : GameComponent
    {

        #region Variables and Properties

        private static GamePadState[] _newPad, _oldPad;

        //Hold the keyboard state
        private static KeyboardState _newK, _oldK;
        //for mouse scroll events
        private static int _newScroll, _oldScroll;
        //for mouse click events
        private static MouseState _newM, _oldM;

        //for double clicks, a bit more difficult to measure
        private static bool _doubleActive, _doubleClicked;
        private static float _doubleCounter, _baseDouble;

        private static Vector2 _mousePos, _oldPos, _zoomOffset;

        private static Rectangle _mouseRect;

        private static Main _game;

        private static Vector2 _firstClickPosition, _currentClickPosition; //for the entity selection rectangle
        private static Rectangle _mouseSelectionRectangle; //the selection rectangle
        private static bool _rectangleMaking = false;

        private static SpriteBatch SpriteBatch { get { return _game.SpriteBatch; } }

        public static Rectangle MouseSelectionRectangle
        {
            get { return _mouseSelectionRectangle; }
        }

        public static Vector2 MouseVelocity
        {
            get { return (_mousePos - _oldPos); }
        }

        public static Vector2 RawMousePosition { get { return _mousePos; } }

        public static Vector2 MousePosition
        {
            get { return _mousePos + _zoomOffset; }
        }

        public static bool DoubleClick
        {
            get { return _doubleClicked; }
        }

        public static bool ScrolledUp
        {
            get { return _newScroll > _oldScroll; }
        }

        public static bool ScrolledDown
        {
            get { return _newScroll < _oldScroll; }
        }


        public static bool LeftClick
        {
            get { return _newM.LeftButton == ButtonState.Pressed && _oldM.LeftButton == ButtonState.Released; }
        }

        public static bool LeftHeld
        {
            get { return _newM.LeftButton == ButtonState.Pressed && _oldM.LeftButton == ButtonState.Pressed; }
        }

        public static bool LeftReleased
        {
            get { return _newM.LeftButton == ButtonState.Released && _oldM.LeftButton == ButtonState.Pressed; }
        }

        public static bool LeftUp
        {
            get { return _newM.LeftButton == ButtonState.Released && _oldM.LeftButton == ButtonState.Released; }
        }

        public static bool RightClick
        {
            get { return _newM.RightButton == ButtonState.Pressed && _oldM.RightButton == ButtonState.Released; }
        }

        public static bool RightHeld
        {
            get { return _newM.RightButton == ButtonState.Pressed && _oldM.RightButton == ButtonState.Pressed; }
        }

        public static bool RightReleased
        {
            get { return _newM.RightButton == ButtonState.Released && _oldM.RightButton == ButtonState.Pressed; }
        }


        public static bool MiddleMouseClick
        {
            get { return _newM.MiddleButton == ButtonState.Pressed && _oldM.MiddleButton == ButtonState.Released; }
        }

        public static bool MouseSelectionRectangleActive
        {
            get { return _rectangleMaking; }
        }

        #endregion

        #region Constructor

        public Input(Game game)
            : base(game)
        {
            _game = ((Main)game);
            // TODO: Construct any child components here
        }

        #endregion

        #region XNA Methods

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            _newPad = new GamePadState[4];
            _oldPad = new GamePadState[4];

            _mouseSelectionRectangle = new Rectangle();
            _firstClickPosition = _currentClickPosition = Vector2.Zero;

            _zoomOffset = Vector2.Zero;

            for (int i = 0; i < 4; i++)
            { // loop over up to 4 gamepads
                _newPad[i] = new GamePadState();
                _oldPad[i] = _newPad[i];
            }

            _newK = Keyboard.GetState();
            _newM = Mouse.GetState();
            _baseDouble = 300;
            _doubleCounter = _baseDouble;
            _doubleActive = false;
            _oldScroll = 0;
            _newScroll = 0;
            _mousePos = new Vector2();
            _oldPos = new Vector2();
            _mouseRect = new Rectangle(0, 0, 2, 2); //2 represents the mouse collision box, change if needed
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            _zoomOffset = (_mousePos - Camera.CentrePos) * (1 - Camera.Zoom);
            _zoomOffset += Camera.CentrePos;
            _zoomOffset -= Main.CentreScreen;
            _zoomOffset *= (1 / Camera.Zoom);

            _doubleClicked = false;
            _oldK = _newK; //Update keyboard events
            _newK = Keyboard.GetState();

            _oldM = _newM; //Update mouse events
            _newM = Mouse.GetState();

            _newScroll = _newM.ScrollWheelValue; //Update mouse scroll events
            _oldScroll = _oldM.ScrollWheelValue;

            _oldPos.X = _oldM.Position.X;
            _oldPos.Y = _oldM.Position.Y;

            _mouseRect.X = (int)(_mousePos.X = _newM.Position.X);
            _mouseRect.Y = (int)(_mousePos.Y = _newM.Position.Y); // Update mouse position

            if (_doubleActive)
            {
                _doubleCounter -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (_doubleCounter < 0)
                {
                    _doubleCounter = _baseDouble;
                    _doubleActive = false;
                }
                if (_doubleActive && Input.LeftClick)
                {
                    _doubleClicked = true;
                    _doubleActive = false;
                    _doubleCounter = _baseDouble;
                }
            }
            else
            {
                if (Input.LeftClick)
                {
                    _doubleActive = true;
                }
            }

            #region Mouse Rectangle Formation

            if (Input.LeftClick)
            {
                _rectangleMaking = true;
                _firstClickPosition = Input.MousePosition;
                _mouseSelectionRectangle = new Rectangle((int)_firstClickPosition.X, (int)_firstClickPosition.Y, 1, 1);

            }
            else
            {
                if (Input.LeftHeld)
                {
                    _currentClickPosition = Input.MousePosition;

                    _mouseSelectionRectangle.Width = Math.Abs((int)_firstClickPosition.X - (int)_currentClickPosition.X) + 2;
                    _mouseSelectionRectangle.Height = Math.Abs((int)_firstClickPosition.Y - (int)_currentClickPosition.Y) + 2;

                    _mouseSelectionRectangle.X = Math.Min((int)_firstClickPosition.X, (int)_currentClickPosition.X);
                    _mouseSelectionRectangle.Y = Math.Min((int)_firstClickPosition.Y, (int)_currentClickPosition.Y);

                }
                else
                {
                    _rectangleMaking = false;
                }
            }

            #endregion

            base.Update(gameTime);
        }

        #endregion

        #region Controller Methods

        public static void VibrateController(int num, float length)
        {
            if (num >= 0 && num <= 4 && _newPad[num].IsConnected)
            {

            }
        }

        public static Vector2 MovementControllerLeft(int num)
        {
            if (num >= 0 && num <= 4 && _newPad[num].IsConnected)
            {
                return new Vector2(_newPad[num].ThumbSticks.Left.X, -_newPad[num].ThumbSticks.Left.Y);
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public static bool LeftTriggerPressed(int num)
        {
            if (num >= 0 && num <= 4 && _newPad[num].IsConnected)
            {
                return _newPad[num].Triggers.Left == 1.0f && _oldPad[num].Triggers.Left < 1.0f;
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public static bool RightTriggerPressed(int num)
        {
            if (num >= 0 && num <= 4 && _newPad[num].IsConnected)
            {
                return _newPad[num].Triggers.Right == 1.0f && _oldPad[num].Triggers.Right < 1.0f;
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public static bool LeftTriggerDown(int num)
        {
            if (num >= 0 && num <= 4 && _newPad[num].IsConnected)
            {
                return _newPad[num].Triggers.Left == 1.0f && _oldPad[num].Triggers.Left == 1.0f;
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public static bool RightTriggerDown(int num)
        {
            if (num >= 0 && num <= 4 && _newPad[num].IsConnected)
            {
                return _newPad[num].Triggers.Right == 1.0f && _oldPad[num].Triggers.Right == 1.0f;
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public static Vector2 MovementControllerRight(int num)
        {
            if (num >= 0 && num <= 4 && _newPad[num].IsConnected)
            {
                return new Vector2(_newPad[num].ThumbSticks.Right.X, -_newPad[num].ThumbSticks.Right.Y);
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public static Vector2 MovementControllerLeft(int num, bool inverted)
        {
            if (num >= 0 && num <= 4 && _newPad[num].IsConnected)
            {
                if (inverted)
                {
                    return _newPad[num].ThumbSticks.Left;
                }
                return new Vector2(_newPad[num].ThumbSticks.Left.X, -_newPad[num].ThumbSticks.Left.Y);
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public static Vector2 MovementControllerRight(int num, bool inverted)
        {
            if (num >= 0 && num <= 4 && _newPad[num].IsConnected)
            {
                if (inverted)
                {
                    return _newPad[num].ThumbSticks.Right;
                }
                return new Vector2(_newPad[num].ThumbSticks.Right.X, -_newPad[num].ThumbSticks.Right.Y);
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }
        

        #endregion

        #region Methods

        public static Keys[] GetPressedKeys()
        {
            return _newK.GetPressedKeys();
        }

        public static void Flush()
        {
            _oldM = _newM;
            _oldK = _newK;
        }

        public static bool KeyDown(Keys key)
        {
            return _newK.IsKeyDown(key) && _oldK.IsKeyDown(key);
        }

        public static bool KeyUp(Keys key)
        {
            return _newK.IsKeyUp(key) && _oldK.IsKeyUp(key);
        }

        public static bool KeyPressed(Keys key)
        {
            return _newK.IsKeyDown(key) && _oldK.IsKeyUp(key);
        }

        public static bool KeyReleased(Keys key)
        {
            return _newK.IsKeyUp(key) && _oldK.IsKeyDown(key);
        }

        public static bool MouseCollision(Rectangle rec)
        {
            return rec.Intersects(_mouseRect) || rec.Contains(_mouseRect);
        }

        public static bool MouseCollision(Vector2 point, float radius)
        {
            return Vector2.Distance(MousePosition, point) < radius;
        }

        #endregion

    }
}