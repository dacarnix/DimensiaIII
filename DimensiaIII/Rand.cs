﻿using System;

using Microsoft.Xna.Framework;

namespace DimensiaIII
{
    public static class Rand
    {

        private static Random rand = new Random();

        public static byte GetRandByte()
        {
            return (byte)rand.Next(255);
        }

        public static int GetInt(int top)
        {
            return rand.Next(top);
        }
        public static int GetInt(int low, int top)
        {
            return rand.Next(low, top);
        }

        public static float GetFloat(int low, int top)
        {
            return (float)rand.NextDouble() * (rand.Next(low, top));
        }

        public static double GetDouble(int low, int top)
        {
            return rand.NextDouble() * (rand.Next(low, top));
        }

        public static double GetDouble(double low, double top)
        {
            return rand.NextDouble() * (top - low) + low;
        }

        public static float GetFloat(float low, float top)
        {
            return (float)(rand.NextDouble() * (top - low) + low);
        }

        public static float GetFloat(double low, double top)
        {
            return (float)(rand.NextDouble() * (top - low) + low);
        }

        public static Color GetColor()
        {
            return new Color(rand.Next(255), rand.Next(255), rand.Next(255));
        }

        public static Color GetSimilarColor(Color c)
        {
            return new Color(rand.Next(Math.Max(c.R - 30, 0), Math.Min(255, c.R + 30)), rand.Next(Math.Max(c.G - 30, 0), Math.Min(255, c.G + 30)), rand.Next(Math.Max(c.B - 30, 0), Math.Min(255, c.B + 30)));
        }

        public static Color GetSimilarColor(Color c, int difference)
        {
            return new Color(rand.Next(Math.Max(c.R - difference, 0), Math.Min(255, c.R + difference)), rand.Next(Math.Max(c.G - difference, 0), Math.Min(255, c.G + difference)), 
                rand.Next(Math.Max(c.B - difference, 0), Math.Min(255, c.B + difference)));
        }


    }
}
