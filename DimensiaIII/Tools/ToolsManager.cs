﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DimensiaIII.Tools
{
    public class ToolsManager : List<Tool>
    {

        #region Fields and Properties

        private int _selectedControl = 0;

        static SpriteFont spriteFont;

        bool _acceptInput = true;

        private Rectangle _clickableArea;

        public bool AcceptInput
        {
            get { return _acceptInput; }
            set { _acceptInput = value; }
        }

        public static SpriteFont SpriteFont
        {
            get { return spriteFont; }
            set { spriteFont = value; }
        }

        public Rectangle ClickableArea { get { return _clickableArea; } }

        #endregion

        #region Event Region

        public event EventHandler focusChanged;

        #endregion

        #region Constructors

        public ToolsManager(SpriteFont spriteFont)
            : base()
        {
            ToolsManager.spriteFont = spriteFont;
            _clickableArea = new Rectangle();
        }

        #endregion

        #region XNA Methods

        public void Update(GameTime gameTime)
        {
            if (gameTime != null)
            {

                if (Count == 0)
                    return;

                if (Input.LeftClick)
                {
                    //clear all focuses
                    foreach (Tool c in this)
                    {
                       // if (c.HasFocus)
                          c.HasFocus = false;
                    }
                }

                foreach (Tool c in this)
                {
                   c.AlwaysUpdate();
                   c.Update(gameTime);
                }

                if (AcceptInput)
                {
                    if (Input.KeyPressed(Keys.Up))
                        PreviousControl();

                    if (Input.KeyPressed(Keys.Down))
                        NextControl();
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Tool c in this)
            {
                if (c.Visible)
                    c.Draw(spriteBatch);
            }
            //MOUSE IS DRAWN HERE
            spriteBatch.Draw(Pipeline.TextureManager.Pixel, new Rectangle((int)Input.RawMousePosition.X, (int)Input.RawMousePosition.Y, 4, 4), Color.Red);
        }

        public void NextControl()
        {
            if (Count == 0)
                return;

            int currentControl = _selectedControl;

            this[_selectedControl].HasFocus = false;

            do
            {
                _selectedControl++;

                if (_selectedControl == Count)
                    _selectedControl = 0;

                if (this[_selectedControl].TabStop && this[_selectedControl].Enabled)
                {
                    focusChanged?.Invoke(this[_selectedControl], null);

                    break;
                }

            } while (currentControl != _selectedControl);

            this[_selectedControl].HasFocus = true;
        }

        public void PreviousControl()
        {
            if (Count == 0)
                return;

            int currentControl = _selectedControl;

            this[_selectedControl].HasFocus = false;

            do
            {
                _selectedControl--;

                if (_selectedControl < 0)
                    _selectedControl = Count - 1;

                if (this[_selectedControl].TabStop && this[_selectedControl].Enabled)
                {
                    focusChanged?.Invoke(this[_selectedControl], null);

                    break;
                }
            } while (currentControl != _selectedControl);

            this[_selectedControl].HasFocus = true;
        }

        #endregion

    }
}
