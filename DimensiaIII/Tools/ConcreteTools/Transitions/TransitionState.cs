﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DimensiaIII.Tools.ConcreteTools.Transitions
{
    /// <summary>
    /// To represent the state animations of opening/closing an expanding box
    /// </summary>
    public enum TransitionState
    {
        Opening,
        Viisible,
        Hiding,
        Hidden
    }
}
