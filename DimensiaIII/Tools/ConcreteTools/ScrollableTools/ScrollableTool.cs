﻿using DimensiaIII.Tools.ConcreteTools.AlignmentOptions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DimensiaIII.Tools.ConcreteTools
{
    /// <summary>
    /// An extensive of ToolBox, allows you to scroll up and down
    /// OR to use the scroll up/down (mouse) to change the selected item by setting scrollable = true
    /// </summary>
    public class ScrollableTool : ToolBox
    {

        #region Variables and Properties

        private int _maxWidth, _maxHeight;

        //Keep track of which tools are visible
        private int _lowTracker, _upTracker;

        //To show where the tracker is on the scrollable items
        private int _activeTracker;

        //Defines the selected element in the list
        private int _selectedElement;

        //Set this true, if you want the scroll to also change the active selected component
        private bool _scrollable;

        private int _totalWidth, _totalHeight;

        //Should this always scroll regardless of what else is focused?
        private bool _alwaysActive;

        private bool _unlimitedWidth, _unlimitedHeight;

        //set this if you want the scroll to loop around at the extremes
        private bool _loopAround;

        private int _heldDown, _heldUp;

        private int _lastTracker;

        private Vector2 _toolScrollOffset;

        private ScrollDirection _scrollDirection;

        private Keys _upScroll, _downScroll;

        public ScrollDirection ScrollingDirection
        {
            get { return _scrollDirection; }
        }

        public int MaxWidth
        {
            get { return _maxWidth; }
        }

        public int MaxHeight
        {
            get { return _maxHeight; }
        }

        public bool Scrollable
        {
            get { return _scrollable; }
            set { _scrollable = value; }
        }

        public bool AlwaysFocus
        {
            get { return _alwaysActive; }
            set { _alwaysActive = value;  }
        }

        public Tool SelectedChild
        {
            get { return _children[_selectedElement]; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset">Gap between succesive elements</param>
        /// <param name="maxWidth">Max height of the box (leave 0 for no limit)</param>
        /// <param name="maxHeight">Max Width of the box (leave 0 for no limit)</param>
        /// <param name="direction">Direction to scroll in</param>
        public ScrollableTool(Vector2 offset, int maxWidth, int maxHeight, ScrollDirection direction) : base(offset)
        {
            this._maxHeight = maxHeight;
            this._maxWidth = maxWidth;
            _scrollDirection = direction;
            _unlimitedHeight = (_maxHeight == 0);
            _unlimitedWidth = (_maxWidth == 0);
            _alwaysActive = false;
            _loopAround = false;
            _lastTracker = 0;
            _activeTracker = 0;
            _selectedElement = 0;
            _toolScrollOffset = Vector2.Zero;
            _heldDown = _heldUp = 0;

            if (direction == ScrollDirection.UpDown)
            {
                _upScroll = Keys.Up;
                _downScroll = Keys.Down;
            } else
            {
                _upScroll = Keys.Left;
                _downScroll = Keys.Right;
            }

        }

        #endregion

        #region XNA Methods

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            _mouseOver = Input.MouseCollision(this._clickableArea);

            if (_children.Count > 0)
            {
                if (_mouseOver || _alwaysActive)
                {
                    if (Input.ScrolledUp || Input.KeyPressed(_upScroll) || (_heldUp > 20 && _heldUp % 6 == 0))
                    {
                        ScrollDown();
                    }
                    if (Input.KeyDown(_upScroll))
                    {
                        _heldUp++;
                    }else
                    {
                        _heldUp = 0;
                    }
                    if (Input.ScrolledDown || Input.KeyPressed(_downScroll) || (_heldDown > 20 && _heldDown % 6 == 0))
                    {
                        ScrollUp();
                    }

                    if (Input.KeyDown(_downScroll))
                    {
                        _heldDown++;
                    }
                    else
                    {
                        _heldDown = 0;
                    }

                    if (_scrollable)
                    {
                        _children[_lastTracker].HasFocus = false;
                        _children[_activeTracker].HasFocus = true;
                    }
                }

                for (int i = _lowTracker; i <= MathHelper.Min(_upTracker, _children.Count - 1); i++)
                {
                    _children[i].ScrollableOffset = _toolScrollOffset;
                    _children[i].Update(gameTime);
                }
            }
            _lastTracker = _activeTracker;

        }

        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(Pipeline.TextureManager.Pixel, ClickableArea, Color.Yellow * .2f);
            for (int i = _lowTracker; i <= MathHelper.Min(_upTracker, _children.Count - 1); i++)
            {
                if (i == _activeTracker)
                {
                    batch.Draw(Pipeline.TextureManager.Pixel, _children[i].ClickableArea, Color.Red * .3f);
                }
                _children[i].Draw(batch);
            }
        }


        #endregion

        #region Scrolling
        /// <summary>
        /// Scroll the active trackers up
        /// </summary>
        private void ScrollUp()
        {
            _activeTracker++;
            if (_activeTracker >= Children.Count)
            {
                _activeTracker = Children.Count - 1;
            }
            UpdateTrackers();
        }

        private void ScrollDown()
        {
            _activeTracker--;
            if (_activeTracker < 0)
            {
                _activeTracker = 0;
            }
            UpdateTrackers();
        }

        /// <summary>
        /// Attempts to update the low/up trackers around the active tracker
        /// </summary>
        private void UpdateTrackers()
        {
            //Looping around is not implemented yet
            if (_lowTracker > 0 && _activeTracker < _lowTracker)
            {
                _lowTracker = _activeTracker;
            }
                

            if (_upTracker < _children.Count - 1 && _activeTracker > _upTracker)
            {
                _upTracker = _activeTracker;
                _lowTracker++;
            }
            CheckTrackers();
        }

        private void SetAtBottom()
        {

        }

        private void SetAtTop()
        {

        }

        /// <summary>
        /// See if we can fit any more elements on the screen
        /// 
        /// Also creates the new bounds to the box, for scrolling on
        /// </summary>
        private void CheckTrackers()
        {
            int minX = 999999999, minY = 999999999;
            _totalHeight = _totalWidth = 0;
            if (_scrollDirection == ScrollDirection.UpDown)
            {
                _upTracker = _lowTracker;
                for (int i = _lowTracker; i < Children.Count; i++)
                {
                    Tool t = Children[i];
                    _totalHeight += t.Height + (int)_succesiveOffset.Y;
                    if (_totalHeight <= _maxHeight)
                        _upTracker++;
                    else
                    {
                        _totalHeight -= t.Height + (int)_succesiveOffset.Y;
                        _upTracker--;
                        break;
                    }

                    minX = MathHelper.Min(minX, (int)t.Position.X);
                    minY = MathHelper.Min(minY, (int)t.Position.Y);

                }

                foreach (Tool t in Children)
                {
                    _totalWidth = MathHelper.Max(_totalWidth, t.Width);
                }
            } else
            {

            }
            _clickableArea.Width = (_scrollDirection == ScrollDirection.UpDown) ? _totalWidth : _maxWidth;
            _clickableArea.Height = (_scrollDirection == ScrollDirection.UpDown) ? _maxHeight : _totalHeight;
            if (_totalHeight > 0)
                _clickableArea.Height -= (int)_succesiveOffset.Y;
            _toolScrollOffset.Y = -(minY - _pos.Y);

            if (_activeTracker > _upTracker)
            {
                _lowTracker++;
                CheckTrackers();
            }


                
        }

        #endregion

        #region Other

        /// <summary>
        /// When added children, we need to see if they'll fit on the screen!
        /// </summary>
        /// <param name="t"></param>
        public override void AddChild(Tool t)
        {
            base.AddChild(t);
            CheckTrackers();
        }

        #endregion

    }
}
