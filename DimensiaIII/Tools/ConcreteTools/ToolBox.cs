﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DimensiaIII.Tools.ConreteTools.AlignmentOptions;
using DimensiaIII.Tools.ConcreteTools.AlignmentOptions;
using DimensiaIII.Tools.ConcreteTools.ExpandableTools;

namespace DimensiaIII.Tools.ConcreteTools
{
    /// <summary>
    /// A composite design pattern to be able to group tools together
    /// and re-position them easier, etc.
    /// 
    /// The toolbox is limited by only HORIZONTAL or VERTICLE offsets, not both.
    /// This will cause weird behaviour
    /// </summary>
    public class ToolBox : Tool
    {
        #region Variables

        protected List<Tool> _children;
        //offset between each tool
        protected Vector2 _succesiveOffset;

        protected ExpandDirection _expandDirection;

        #endregion

        #region Properties

        public override Vector2 ScrollableOffset
        {
            get
            {
                return base.ScrollableOffset;
            }

            set
            {
                base.ScrollableOffset = value;
                foreach (Tool t in _children)
                {
                    t.ScrollableOffset = value;
                }
            }
        }

        public override Vector2 Position
        {
            get
            {
                return base.Position;
            }

            set
            {
                base.Position = value;
                Vector2 trackinPos = value;
                for (int i = 0; i < Children.Count; i++)
                {
                    Tool t = Children[i];
                    bool isLast = i + 1 == Children.Count;
                    //update the tools accordingly

                    t.Position = new Vector2(trackinPos.X, trackinPos.Y);

                    if (_succesiveOffset.Y != 0)
                    {
                        if (_expandDirection == ExpandDirection.Up && !isLast)
                        {
                            trackinPos -= new Vector2(0, Children[i + 1].Height);
                        }
                        else if (_expandDirection == ExpandDirection.Down && !isLast)
                        {
                            trackinPos += new Vector2(0, t.Height);
                        }
                        trackinPos += (_expandDirection == ExpandDirection.Up) ? -(_succesiveOffset) : (_succesiveOffset);
                    }
                    else
                    {
                        trackinPos += (_succesiveOffset);
                    }
                }
            }
        }

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
        }

        public List<Tool> Children
        {
            get { return _children; }
        }

        public override ToolsAlignment Alignment
        {
            get
            {
                return base.Alignment;
            }

            set
            {
                base.Alignment = value;
                foreach (Tool t in _children)
                {
                    t.Alignment = value;
                }
            }
        }

        public override bool HasFocus
        {
            get
            {
                return base.HasFocus;
            }

            set
            {
                base.HasFocus = value;
                if (!value)
                {
                    foreach (Tool t in _children)
                    {
                        t.HasFocus = value;
                    }
                }
            }
        }

        public ExpandDirection ExpandingDirection
        {
            get { return _expandDirection; }
            set {
                _expandDirection = value;
                //Re-update the positions
                Position = Position;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset">Defines the pixels off between each tool in the box</param>
        public ToolBox(Vector2 offset) : base()
        {
            _children = new List<Tool>();
            _succesiveOffset = offset;
            _expandDirection = ExpandDirection.Down;
        }

        #endregion

        #region XNA Methods

        public override void Update(GameTime gameTime)
        {
            UpdateToolBoxOffset();
            foreach (Tool t in _children)
            {
                t.ToolBoxOffset = _toolboxOffset;
                if (this is ScrollableTool == false)
                {
                    t.AlwaysUpdate();
                    t.Update(gameTime);
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch batch)
        {

            foreach (Tool t in _children)
            {
                if (t.Visible)
                  t.Draw(batch);
            }
        }

        #endregion

        #region Other

        public virtual void AddChild(Tool t)
        {
            _children.Add(t);
        }

        private void UpdateToolBoxOffset()
        {
            _width = 0;
            int minX = 90000000, maxX = 0;
            int minY = 90000000, maxY = 0;
            foreach (Tool t in Children)
            {
                minX = Math.Min(minX, (int)t.Position.X);
                maxX = Math.Max(maxX, (int)t.Position.X + t.Width);
                minY = Math.Min(minY, (int)t.Position.Y);
                maxY = Math.Max(maxY, (int)t.Position.Y + t.Height);
            }
            _width = maxX - minX;
            _height = maxY - minY;
            //Horizontal offsets
            if ((int)_succesiveOffset.X != 0 && Children.Count > 0)
            {
                if (_alignment == ToolsAlignment.Centre)
                {
                    _toolboxOffset.X = -(_width / 2) + (Children[0].Width / 2);
                    _toolboxOffset.Y = -(Children[0].Height / 2);
                }
                else if (_alignment == ToolsAlignment.LeftTop)
                {
                    _toolboxOffset.X = 0;
                }
                else if (_alignment == ToolsAlignment.LeftCentre)
                {
                    _toolboxOffset.X = 0;
                    _toolboxOffset.Y = -(Children[0].Height / 2);
                }
            }
        }

        public bool AnythingFocused()
        {
            bool focused = false;
            foreach (Tool t in Children)
            {
                if (t is ExpandableTool)
                {
                    focused = focused | ((ExpandableTool)t).AnythingFocused();
                }
                else if (t is ToolBox)
                {
                    focused = focused | ((ToolBox)t).AnythingFocused();
                }
                else
                {
                    focused = focused | t.HasFocus | t.PreviouslyFocused;
                }
            }
            return focused;
        }

        #endregion
    }
}
