﻿namespace DimensiaIII.Tools.ConcreteTools.AlignmentOptions
{
    /// <summary>
    /// Currently only specific to TextBox
    /// </summary>
    public enum TextAlignment
    {
        Left,
        Centre,
        Right
    }
}
