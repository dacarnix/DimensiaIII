﻿namespace DimensiaIII.Tools.ConreteTools.AlignmentOptions
{
    /// <summary>
    /// Used to align text, generally, for a tool
    /// LeftTop -> Position will be anchored at top-left (THIS IS DEFAULT)
    /// Centre -> Position will be anchored at middle
    /// LeftCentre etc etc
    /// </summary>
    public enum ToolsAlignment
    {
        LeftTop,
        Centre,
        LeftCentre
    }
}
