﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DimensiaIII.Tools.ConcreteTools.AlignmentOptions
{
    /// <summary>
    /// Defines the direction for the boxes to scroll in
    /// </summary>
    public enum ScrollDirection
    {
        UpDown,
        LeftRight
    }
}
