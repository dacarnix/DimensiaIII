﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DimensiaIII.Tools.ConcreteTools.AlignmentOptions
{
    /// <summary>
    /// Defines the direction that a tool box will expand in when an expandable tool is clicked on.
    /// IE, it may be DOWN if the tool is above the mid-Y on the screen, or down else.
    /// </summary>
    public enum ExpandDirection
    {
        Up,
        Down
    }
}
