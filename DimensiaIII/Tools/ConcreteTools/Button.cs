﻿using DimensiaIII.Tools.ConcreteTools.AlignmentOptions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Tools.ConcreteTools
{
    /// <summary>
    /// A basic text button to display on the screen
    /// </summary>
    public class Button : Tool
    {

        #region Variables and Properties

        private bool _showBackground;

        public bool ShowBackground
        {
            get { return _showBackground; }
            set { _showBackground = value; }
        }

        #endregion

        #region Constructor

        public Button(string text, int width, int height, TextAlignment textAlignment) : base()
        {
            Text = text;
            _width = width;
            _height = height;
            _textAlignment = textAlignment;
            _showBackground = true;
        }

        #endregion

        #region Methods
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch batch)
        {
            Vector2 localOffset = new Vector2(0, _height / 2 - (_size.Y / 2)) + base.GetTextAlignmentOffset() + ToolBoxOffset;
            Vector2 toDrawAt = new Vector2(_clickableArea.X, _clickableArea.Y);
            //draw At already contains the scrollable offset
            if (_showBackground)
            {
                if (!_mouseOver)
                {
                    batch.Draw(Pipeline.TextureManager.Pixel, _clickableArea, new Color(85, 85, 85));
                }
                else
                {
                    batch.Draw(Pipeline.TextureManager.Pixel, _clickableArea, new Color(45, 45, 45));
                }
            }
            batch.DrawString(_font, _text, toDrawAt + localOffset, Colour);
        }


        #endregion

    }
}
