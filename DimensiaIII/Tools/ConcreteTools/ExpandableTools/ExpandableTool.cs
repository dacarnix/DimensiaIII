﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DimensiaIII.Tools.ConreteTools.AlignmentOptions;
using DimensiaIII.Tools.ConcreteTools.AlignmentOptions;
using System.Collections.Generic;

namespace DimensiaIII.Tools.ConcreteTools.ExpandableTools
{
    /// <summary>
    /// Adds the ability for 'tools' to appear to the left-down from a given tool
    /// when clicked on (Essentially like a menu bar in windows)
    /// 
    /// Generally the 'wrap tool' is meant to be a button, but it can be anything.
    /// 
    /// This mimicks the decorator pattern
    /// </summary>
    public class ExpandableTool : Tool
    {

        #region Variables and Properties

        //The tool to 'wrap' this expandable functionality around
        private Tool _wrap;
        //this will hold the children to expand
        private ToolBox _box;

        public List<Tool> Children
        {
            get { return _box.Children; }
        }

        public override Vector2 Position
        {
            get
            {
                return base.Position;
            }

            set
            {
                base.Position = value;
                _wrap.Position = value;
                _box.Position = value  + new Vector2(20 + _wrap.Width, 0);
            }
        }

        public override ToolsAlignment Alignment
        {
            get
            {
                return base.Alignment;
            }

            set
            {
                base.Alignment = value;
                _wrap.Alignment = value;
            }
        }

        public override bool HasFocus
        {
            get
            {
                return _wrap.HasFocus;
            }

            set
            {
                _wrap.HasFocus = value;
                if (!value)
                    _box.HasFocus = value;
            }
        }

        public override bool Visible
        {
            get
            {
                return _wrap.Visible;
            }

            set
            {
                _wrap.Visible = value;
            }
        }

        public override bool Active
        {
            get
            {
                return base.Active;
            }

            set
            {
                if (!value)
                {
                    //deselect any child expandable boxes
                    foreach (Tool t in Children)
                    {
                        if (t is ExpandableTool)
                            t.Active = false;
                        t.Visible = false;
                        t.HasFocus = false;
                    }
                }
                else
                {
                    foreach (Tool t in Children)
                    {
                        t.Visible = true;
                    }
                }
                base.Active = value;
            }
        }

        #endregion

        #region Constructor

        public ExpandableTool(Tool toWrap) : base()
        {
            if (toWrap == null)
                throw new ArgumentNullException();
            _wrap = toWrap;
            _box = new ToolBox(new Vector2(0, 40));
            _box.Alignment = ToolsAlignment.Centre;
            _box.Visible = false;
        }

        #endregion

        #region XNA Methods

        public override void Update(GameTime gameTime)
        {
            _width = _wrap.Width;
            _height = _wrap.Height;
            _wrap.Update(gameTime);

            if (_clickableArea.Center.Y > Main.ScreenHeight / 2)
            {
                //Bottom half of screen
                _box.ExpandingDirection = ExpandDirection.Up;
            } else
            {
                _box.ExpandingDirection = ExpandDirection.Down;
            }

            _box.Update(gameTime);

            base.Update(gameTime);

            if (!AnythingFocused() && _active)
            {
                Active = false;
            }

            _previouslyVisible = _active;
        }

        public override void Draw(SpriteBatch batch)
        {
            _wrap.Draw(batch);
            if (_active)
                _box.Draw(batch);
        }

        #endregion

        #region Methods

        protected override void OnSelected(EventArgs e)
        {

            Active = !_active;
        }

        public void AddChild(Tool t)
        {
            _box.AddChild(t);
        }



        /// <summary>
        /// Allows you to set the expandable regions tools to a certain alignment
        /// </summary>
        /// <param name="align"></param>
        public void SetExpandableRegionAlignment(ToolsAlignment align)
        {
            foreach (Tool t in _box.Children)
            {
                t.Alignment = align;
            }
        }

        public bool AnythingFocused ()
        {
            bool focused = false;
            foreach (Tool t in Children)
            {
                if (t is ExpandableTool)
                {
                    focused = focused | ((ExpandableTool)t).AnythingFocused();
                }
                else if (t is ToolBox)
                {
                    focused = focused | ((ToolBox)t).AnythingFocused();
                }
                else
                {
                    focused = focused | t.HasFocus | t.PreviouslyFocused;
                }
            }
            return focused | _wrap.HasFocus | _wrap.PreviouslyFocused;
        }

        #endregion
    }
}
