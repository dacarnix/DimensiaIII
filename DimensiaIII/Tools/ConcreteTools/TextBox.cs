﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using DimensiaIII.Tools.ConcreteTools.AlignmentOptions;

namespace DimensiaIII.Tools.ConcreteTools
{
    public class TextBox : Tool
    {

        #region Variables and Properties

        private char _under = '_';
        private float _blink = 500f;

        private bool _alwaysFocus = false, _flashVis = true, _numbersOnly = false, _backgroundVis;

        private int _maxCharacters;
        private string _defaultText = "";

        //Keyboard input for XNA SUCKS, so we have to have special mappings
        private static Dictionary<int, string> _numMapping;
        private static Dictionary<string, Tuple<string, string>> _specialMappings;

        private Dictionary<Keys, int> _keyTimers;

        public int MaxChars
        {
            get { return _maxCharacters; }
            set { _maxCharacters = value; }
        }  

        public bool AlwaysFocus
        {
            get { return _alwaysFocus; }
            set { _alwaysFocus = value; }
        }

        #endregion

        #region Constructor

        public TextBox(TextAlignment textAlign) : base()
        {
            _tabStop = false;
            _text = "";
            _width = 265;
            _height = 70;
            _backgroundVis = true;
            _maxCharacters = 20;
            _textAlignment = textAlign;
            _textAlignmentOffset = Vector2.Zero;
            _keyTimers = new Dictionary<Keys, int>();

            #region Adding the number mappings
            _numMapping = new Dictionary<int, string>();
            _numMapping[0] = ")";
            _numMapping[1] = "!";
            _numMapping[2] = "@";
            _numMapping[3] = "#";
            _numMapping[4] = "$";
            _numMapping[5] = "%";
            _numMapping[6] = "^";
            _numMapping[7] = "&";
            _numMapping[8] = "*";
            _numMapping[9] = "(";
            #endregion

            #region Special Mappings
            _specialMappings = new Dictionary<string, Tuple<string, string>>();
            _specialMappings["OemQuestion"] = new Tuple<string, string>("/", "?");
            _specialMappings["OemPlus"] = new Tuple<string, string>("=", "+");
            _specialMappings["OemMinus"] = new Tuple<string, string>("-", "_");
            _specialMappings["OemComma"] = new Tuple<string, string>(",", "<");
            _specialMappings["OemPeriod"] = new Tuple<string, string>(".", ">");
            _specialMappings["OemSemicolon"] = new Tuple<string, string>(";", ":");
            _specialMappings["OemQuotes"] = new Tuple<string, string>("'", "\"");
            _specialMappings["OemOpenBrackets"] = new Tuple<string, string>("[", "{");
            _specialMappings["OemCloseBrackets"] = new Tuple<string, string>("]", "}");
            _specialMappings["OemPipe"] = new Tuple<string, string>("\\", "|");
            #endregion
        }

        #endregion

        #region Methods

        private void ToggleFlash()
        {
            _flashVis = (_flashVis) ? false : true;
        }

        private void Blink(GameTime game)
        {
            _blink -= (float)game.ElapsedGameTime.TotalMilliseconds;

            if (_blink < 0)
            {
                ToggleFlash();
                _blink = 500f;
            }

        }

        public override void Update(GameTime gameTime)
        {

           if (!_hasFocus) _flashVis = false;

           if (_alwaysFocus) _hasFocus = true;

           if (HasFocus) Blink(gameTime);
           base.Update(gameTime);

        }

        public override void Draw(SpriteBatch batch)
        {
            Vector2 localOffset = new Vector2(0, _height / 2 - (_size.Y / 2)) + base.GetTextAlignmentOffset() + ToolBoxOffset;
            Vector2 toDrawAt = new Vector2(_clickableArea.X, _clickableArea.Y);
            if (_backgroundVis)
            {
                if (!_hasFocus)
                {
                    batch.Draw(Pipeline.TextureManager.Pixel, _clickableArea, new Color(85, 85, 85));
                }
                else
                {
                    batch.Draw(Pipeline.TextureManager.Pixel, _clickableArea, new Color(45, 45, 45));
                }
            }
            batch.DrawString(_font, _text, toDrawAt + localOffset, Colour);
            if (_flashVis) batch.DrawString(_font, _under.ToString(), new Vector2(_clickableArea.X + localOffset.X + 2 + (_size.X), _clickableArea.Center.Y - 10), _color);
        }

        public override void HandleInput()
        {
            if (Input.KeyDown(Keys.LeftControl) && Input.KeyPressed(Keys.V))
            {
                string clip = System.Windows.Forms.Clipboard.GetText();
                if (_text.Length + clip.Length < _maxCharacters)
                   _text += clip;
                else
                {
                    _text += clip.Substring(0, Math.Max(0, _maxCharacters - _text.Length));
                }
            }
            else
            {
                bool justShiftDown = Input.KeyDown(Keys.LeftShift) ^ Input.KeyDown(Keys.RightShift);
                bool shiftDown = (justShiftDown ^ System.Windows.Forms.Control.IsKeyLocked(System.Windows.Forms.Keys.CapsLock));

                foreach (Keys k in Enum.GetValues(typeof(Keys)))
                {
                    if (!_keyTimers.ContainsKey(k))
                        _keyTimers[k] = 0;
                    if (Input.KeyPressed(k) || (_keyTimers[k] > 20 && _keyTimers[k] % 3 == 0 && Input.KeyDown(k)))
                    {
                        if (k.ToString().Equals("Back"))
                        {
                            if (_text == _defaultText && !_defaultText.Equals("")) _text = _text.Remove(0);
                            else if (_text.Length != 0) _text = _text.Remove(_text.Length - 1);

                        }

                        else if (_text.Length == _maxCharacters)
                        {
                            return;
                        }

                        else if (k.ToString().Equals("Space"))
                        {
                            _text += " ";
                        }
                        else if ((k == Keys.OemPeriod || k == Keys.Decimal) && !justShiftDown)
                        {
                            _text += ".";
                        }

                        else
                        {

                            for (int i = 0; i < 10; i++)
                            {
                                if (k.ToString().Equals("D" + i.ToString()))
                                {
                                    if (justShiftDown && !_numbersOnly)
                                    {
                                        _text += _numMapping[i];
                                    }
                                    else _text += i.ToString();
                                    break;
                                }
                                else if (k.ToString().Equals("NumPad" + i.ToString()))
                                {
                                    _text += i.ToString();
                                    break;
                                }
                            }
                            if (!_numbersOnly)
                            {
                                string kS = k.ToString();
                                if (_specialMappings.ContainsKey(kS))
                                {
                                    if (justShiftDown) _text += _specialMappings[kS].Item2;
                                    else _text += _specialMappings[kS].Item1;
                                }
                                else if (kS.Length == 1)
                                {
                                    if (shiftDown) _text += kS;
                                    else _text += kS.ToLower();
                                }
                            }
                        }
                    }
                    if (Input.KeyDown(k))
                    {
                        _keyTimers[k]++;
                    }
                    else
                    {
                        _keyTimers[k] = 0;
                    }

                }
            }
            Text = _text;
        }

        #endregion

    }
}
