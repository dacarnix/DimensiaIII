﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Tools.ConcreteTools
{
    /// <summary>
    /// A basic picturebox to display a texture2D on the screen
    /// </summary>
    public class PictureBox : Tool
    {

        #region Variables and Properties

        private Texture2D _texture;

        #endregion

        #region Constructor

        public PictureBox(Texture2D texture) : base()
        {
            _tabStop = false;
            _texture = texture;
        }

        #endregion

        #region Methods

        public override void Update(GameTime gameTime)
        {
            _width = (int)_size.X;
            _height = (int)_size.Y;
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch batch)
        {
            if (this._visible)
            {
                batch.DrawString(_font, _text, _pos + _offset + ((_size * _scale) / 2) + ToolBoxOffset, _color * _alpha, 0f, _size / 2, _scale, SpriteEffects.None, 1f);
            }
        }


        #endregion

    }
}
