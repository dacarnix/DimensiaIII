﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Tools.ConcreteTools
{
    /// <summary>
    /// A basic text label to display on the screen
    /// </summary>
    public class Label : Tool
    {

        #region Variables and Properties

        public override Vector2 Size
        {
            get
            {
                return base.Size;
            }

            protected set
            {
                base.Size = value;
                _width = (int)_size.X;
                _height = (int)_size.Y;
            }
        }

        #endregion

        #region Constructor

        public Label(string text) : base()
        {
            _tabStop = false;
            Text = text;
        }

        #endregion

        #region Methods

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch batch)
        {
            if (this._visible)
            {
                batch.DrawString(_font, _text, _pos + _scrollableOffset + _offset + ((_size * _scale) / 2) + ToolBoxOffset, _color * _alpha, 0f, _size / 2, _scale, SpriteEffects.None, 1f);
            }
        }


        #endregion

    }
}
