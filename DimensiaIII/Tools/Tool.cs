﻿ using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DimensiaIII.Tools.ConreteTools.AlignmentOptions;
using DimensiaIII.Tools.ConcreteTools.AlignmentOptions;
using DimensiaIII.Tools.ConcreteTools;

namespace DimensiaIII.Tools
{
    public abstract class Tool
    {

        #region Fields

        protected ToolsAlignment _alignment;
        //Based on the alignment, the offset will correctly be calculated
        //to draw respectively.
        protected Vector2 _offset, _toolboxOffset;

        private bool _updateClickArea = true;

        protected SpriteFont _font;
        protected bool _tabStop, _mouseOver, _enabled, _visible, _hasFocus;
        protected Vector2 _pos, _size;

        protected Color _color;

        protected string _text;

        protected float _scale, _alpha;

        protected Rectangle _clickableArea;

        protected int _width, _height;

        public event EventHandler Selected;

        //Only used in textbox and button atm
        protected TextAlignment _textAlignment;
        protected Vector2 _textAlignmentOffset, _scrollableOffset;

        protected bool _previouslyVisible;

        protected bool _active;

        protected bool _previouslyFocused = false;

        #endregion

        #region Property Region

        public virtual Rectangle ClickableArea
        {
            get { return _clickableArea; }
        }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public bool PreviouslyFocused
        {
            get { return _previouslyFocused; }
            set { _previouslyFocused = value; }
        }

        public bool PreviouslyVisible
        {
            get { return _previouslyVisible; }
        }

        public virtual TextAlignment TextAlignment
        {
            get { return _textAlignment; }
            set { _textAlignment = value; }
        }

        public Vector2 ToolBoxOffset
        {
            get { return _toolboxOffset; }
            set { _toolboxOffset = value; }
        }

        public virtual ToolsAlignment Alignment
        {
            get { return _alignment; }
            set
            {
                _alignment = value;
            }
        }

        public string Text
        {
            get { return _text; }
            set { 
                     _text = value;
                     Size = _font.MeasureString(value);
                }
        }


        public virtual Vector2 Position
        {
            get { return _pos; }
            set {
                _pos = value;
            }
        }

        public virtual Vector2 ScrollableOffset
        {
            get { return _scrollableOffset; }
            set { _scrollableOffset = value; }
        }

        public virtual bool HasFocus
        {
            get { return _hasFocus; }
            set { _hasFocus = value; }
        }

        public virtual Vector2 Size
        {
            get { return _size; }
            protected set {
                _size = value;
            }
        }


        public virtual bool MouseOver
        {
            get { return _mouseOver; }
            set { _mouseOver = value; }
        }

        public virtual bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }

        public bool TabStop
        {
            get { return _tabStop; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public Color Colour
        {
            get { return _color; }
            set { _color = value; }
        }

        public SpriteFont SpriteFont
        {
            get { return _font; }
            set { _font = value; }
        }

        public int Width
        {
            get { return _width; }
        }

        public int Height
        {
            get { return _height; }
        }

        #endregion

        #region Constructor

        public Tool()
        {
            Colour = Color.White;
            _enabled = true;
            _visible = true;
            _font = ToolsManager.SpriteFont;
            _clickableArea = new Rectangle();
            _alpha = _scale = 1;
            _offset = Vector2.Zero;
            _alignment = ToolsAlignment.LeftTop;
            _toolboxOffset = Vector2.Zero;
            _textAlignment = TextAlignment.Centre;
            _textAlignmentOffset = Vector2.Zero;
            _active = false;
            _scrollableOffset = Vector2.Zero;
        }

        #endregion

        #region Abstract Methods

        /// <summary>
        /// Called always even if not visible
        /// </summary>
        public virtual void AlwaysUpdate()
        {
            ReAlign();
            _clickableArea.X = (int)Position.X + (int)_offset.X + (int)ToolBoxOffset.X + (int)_scrollableOffset.X;
            _clickableArea.Y = (int)Position.Y + (int)_offset.Y + (int)ToolBoxOffset.Y + (int)_scrollableOffset.Y;
            if (this is ScrollableTool == false)
            {
                _clickableArea.Width = _width;
                _clickableArea.Height = _height;
            }

            _alpha = MathHelper.Clamp(_alpha, 0, 1);
        }

        /// <summary>
        /// Generally not called if this tool is not visible.
        /// Handles clicks, and input
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {
            AlwaysUpdate();
            if (Input.MouseCollision(_clickableArea) && this is ToolBox == false)
            {
                _mouseOver = true;
            }
            else
            {
                _mouseOver = false;
            }
            if (_visible || _previouslyVisible)
            {

                if (MouseOver && Input.LeftClick && !_previouslyFocused)
                {
                    HasFocus = true;
                    OnSelected(null);
                }

                if (((HasFocus) && Input.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Enter)))
                {
                    this.OnSelected(null);
                }

                if (HasFocus && Visible)
                    HandleInput();
            }

            _previouslyFocused = _hasFocus;

        }

        public abstract void Draw(SpriteBatch batch);

        #endregion

        #region Virtual Methods

        public virtual void HandleInput()
        {

        }

        protected virtual void OnSelected(EventArgs e)
        {
            Selected?.Invoke(this, e);
        }

        /// <summary>
        /// Updates the scale of the text to ensure that the text/label
        /// will be a certain width / height
        /// </summary>
        /// <param name="width"></param>
        public virtual void SetWidthByPixel(int width)
        {
            
        }

        public virtual void SetHeightByPixel(int height)
        {
            
        }

        /// <summary>
        /// Re-positions based off of the new alignment
        /// </summary>
        public virtual void ReAlign()
        {
            #region LeftTop
            if (_alignment == ToolsAlignment.LeftTop)
            {
                _offset = Vector2.Zero; // default
            }
            #endregion
            #region Centre
            else if (_alignment == ToolsAlignment.Centre)
            {
                _offset = new Vector2(-_width / 2, -_height / 2);
            }
            #endregion
            #region LeftCentre
            else if (_alignment == ToolsAlignment.LeftCentre)
            {
                _offset = new Vector2(0, -_height / 2);
            }
            #endregion
        }

        #endregion

        #region Other

        protected Vector2 GetTextAlignmentOffset()
        {
            if (_textAlignment == TextAlignment.Left)
            {
                _textAlignmentOffset.X = 10;
            }
            else if (_textAlignment == TextAlignment.Centre)
            {
                _textAlignmentOffset.X = (_width / 2) - (_size.X / 2);
            }
            else if (_textAlignment == TextAlignment.Right)
            {
                _textAlignmentOffset.X = _width - 15 - (_size.X);
            }
            return _textAlignmentOffset;
        }


        #endregion
    }
}
