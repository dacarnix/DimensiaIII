﻿
using Microsoft.Xna.Framework;

namespace DimensiaIII
{

    public static class Collisions
    {

        #region Variables and Properties

        #endregion

        #region Set Up Methods

        
        #endregion

        #region Methods

        /// <summary>
        /// This is a faster ABS method, as it only deals with ints.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static int Abs(int value)
        {
            return (value < 0) ? -value : value;
        }


        #region Circle Collisions
        //source: http://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection/402010#402010
        public static bool CircleRectangleCollision(Point circleCenter, int radius, Rectangle rect)
        {

            int circleX = Abs(circleCenter.X - rect.X);
            int circleY = Abs(circleCenter.Y - rect.Y);

            if (circleX > (rect.Width / 2 + radius)) { return false; }
            if (circleY > (rect.Height / 2 + radius)) { return false; }

            if (circleX <= (rect.Width / 2)) { return true; }
            if (circleY <= (rect.Height / 2)) { return true; }

            return ((((circleX - rect.Width / 2) * (circleX - rect.Width / 2)) +
                ((circleY - rect.Height / 2) * (circleY - rect.Height / 2)))
                <= (radius * radius)) ;
        }

        public static bool CircleRectangleCollision(Vector2 circleCenter, int radius, Rectangle rect)
        {
            return CircleRectangleCollision(circleCenter.ToPoint(), radius, rect);
        }

        public static bool CircleCircleCollision(Vector2 centre, int rad, Vector2 centreTwo, int radTwo)
        {
            return CircleCircleCollision(centre.ToPoint(), rad, centreTwo.ToPoint(), radTwo);
        }

        public static bool CircleCircleCollision(Point centre, int rad, Point centreTwo, int radTwo)
        {
            return (((centre.X - centreTwo.X) * (centre.X - centreTwo.X)
                    + (centre.Y - centreTwo.Y) * (centre.Y - centreTwo.Y))
                    <= (rad + radTwo) * (rad + radTwo));
        }

        #endregion

        #endregion

    }
}
