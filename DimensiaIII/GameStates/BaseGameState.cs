﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using DimensiaIII.Tools;

namespace DimensiaIII.GameStates
{

    public enum ChangeType { Change, Pop, Push }

    public abstract partial class BaseGameState : GameState
    {
        #region Fields

        protected Main game;

        protected SpriteBatch SpriteBatch
        {
            get { return game.SpriteBatch; }
        }

        protected ToolsManager tools;

        #endregion

        #region Constructor Region

        public BaseGameState(Game game, GameStateManager manager)
            : base(game, manager)
        {
            this.game = (Main)game;
        }

        #endregion

        #region XNA methods

        protected override void LoadContent()
        {
            tools = new ToolsManager(Pipeline.FontManager.Clean);
            base.LoadContent();
        }

        protected override void UnloadContent()
        {
            try
            {
                Game.Content.Unload();
            }
            catch { }
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {

            if (this.game.IsActive)
            {
                tools.Update(gameTime);
                base.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {

            base.Draw(gameTime);

        }

        public virtual void Transition(ChangeType change, BaseGameState gameState)
        {

        }

        #endregion
    }
}
