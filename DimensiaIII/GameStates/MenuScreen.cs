﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DimensiaIII.Tools;
using DimensiaIII.Tools.ConcreteTools;


namespace DimensiaIII.GameStates
{
    //Menu screen - where pretty much everything is connected to.
    //Can call out to any screen (not title, however.)

    public class MenuScreen : BaseGameState
    {

        #region Variables


        #endregion

        #region Constructor

        public MenuScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {

        }

        #endregion

        #region XNA methods

        protected override void LoadContent()
        {
            base.LoadContent();
            Tool l = new Label("Play");
            l.Position = new Vector2(200, 200);
            l.Selected += l_Selected;
            tools.Add(l);
        }

        void l_Selected(object sender, EventArgs e)
        {
            StateManager.ChangeState(game.GameSelectScreen);
        }

        public override void ReloadLabels()
        {
            base.ReloadLabels();
        }

        public override void Update(GameTime gameTime)
        {
            if (game.IsActive)
            {
                
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch.Begin();
            tools.Draw(SpriteBatch);
            SpriteBatch.End();
            base.Draw(gameTime);
        }

        #endregion

        #region Methods



        #endregion

    }
}
