﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DimensiaIII.Debug;

namespace DimensiaIII.GameStates
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public abstract partial class GameState : Microsoft.Xna.Framework.DrawableGameComponent
    {
        #region Fields and Properties

        List<GameComponent> childComponents;
        protected BaseDebug _debug;
        public List<GameComponent> Components
        {
            get { return childComponents; }
        }

        GameState tag;

        public GameState Tag
        {
            get { return tag; }
        }

        protected GameStateManager StateManager;

        #endregion

        #region Constructor Region

        public GameState(Game game, GameStateManager manager)
            : base(game)
        {
            StateManager = manager;

            childComponents = new List<GameComponent>();
            tag = this;
            _debug = GetDebugState();
            _debug.SetGame(game);
        }

        #endregion

        #region Factory Method

        protected virtual BaseDebug GetDebugState()
        {
            return new BaseDebug();
        }

        #endregion

        #region XNA Drawable Game Component Methods

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void UnloadContent()
        {
            

            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (GameComponent component in childComponents)
            {
                if (component.Enabled)
                    component.Update(gameTime);
            }

            base.Update(gameTime);
            _debug.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {

            foreach (GameComponent component in childComponents)
            {
                if (component is DrawableGameComponent)
                {
                    if (((DrawableGameComponent)component).Visible)
                        ((DrawableGameComponent)component).Draw(gameTime);
                }
            }

            base.Draw(gameTime);
            _debug.Draw(gameTime);
        }

        #endregion

        #region GameState Method Region

        internal protected virtual void StateChange(object sender, EventArgs e)
        {
            if (StateManager.CurrentState == Tag)
                Show();
            else
                Hide();
        }

        protected virtual void Show()
        {
            Visible = true;
            Enabled = true;
            foreach (GameComponent component in childComponents)
            {
                component.Enabled = true;
                if (component is DrawableGameComponent)
                    ((DrawableGameComponent)component).Visible = true;
            }
        }

        public virtual void ReloadLabels()
        {

        }

        protected virtual void Hide()
        {
            Visible = false;
            Enabled = false;
            foreach (GameComponent component in childComponents)
            {
                component.Enabled = false;
                if (component is DrawableGameComponent)
                    ((DrawableGameComponent)component).Visible = false;
            }
        }

        public void PulseScreenshot(bool success)
        {
            _debug.PulseScreenshot(success);
        }

        #endregion
    }
}
