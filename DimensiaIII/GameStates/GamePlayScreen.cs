﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DimensiaIII.Effects;
using DimensiaIII.Entities;
using DimensiaIII.Networking;
using DimensiaIII.WorldEngine;
using System.Diagnostics;
using DimensiaIII.Debug;

namespace DimensiaIII.GameStates
{
    public class GamePlayScreen : BaseGameState
    {

        #region Variables

        #region Diagnostic Variables

        //average will be in last SECOND
        private static float _timerDraw = 0, drawCounter = 0, drawAverage = 0, drawTicks = 0;
        private static float _timerUpdate = 0, updateCounter = 0, updateAverage = 0, updateTicks = 0;
        private static int lastSecond = 0;


        public static float AverageSecondDrawCycle
        {
            get { return drawAverage; }
        }

        public static float AverageSecondUpdateCycle
        {
            get { return updateAverage; }
        }


        #endregion


        #endregion

        #region Constructor

        public GamePlayScreen(Game game, GameStateManager manager) : base(game, manager)
        {
            
        }

        #endregion

        #region Load, and Update

        #region Load content and Set up

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        #endregion

        public override void ReloadLabels()
        {
            base.ReloadLabels();
        }

        #region Update

        public override void Update(GameTime gameTime)
        {
            Stopwatch s = Stopwatch.StartNew();

            if (NetworkGame.IsNetworkGame) //Make sure this updates first, too.
            {   //potentially could even multithread this, since it is only doing UPDATES.
                NetworkGame.Update(gameTime); //This should always update.
            }

            if (game.IsActive)
            {
                Camera.Update(gameTime);
                World.Update(gameTime);
                EffectManager.Update(gameTime);
                EntityManager.Update(gameTime);
            }
            base.Update(gameTime);

            #region Diagnostics

            s.Stop();
            updateCounter += s.ElapsedMilliseconds;
            updateTicks++;
            if (lastSecond < (int)gameTime.TotalGameTime.TotalSeconds)
            {
                lastSecond = (int)gameTime.TotalGameTime.TotalSeconds;
                drawAverage = (float)drawCounter / (float)drawTicks;
                updateAverage = (float)updateCounter / (float)updateTicks;
                drawTicks = updateTicks = 0; //reset
                updateCounter = drawCounter = 0;
            }

            #endregion
        }


        #endregion

        #endregion

        #region Draw

        public override void Draw(GameTime gameTime)
        {
            Stopwatch s = Stopwatch.StartNew();

            World.WorldMap.CheckRenders(SpriteBatch);

            SpriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera.GetTransformation());

            DrawBack(SpriteBatch);
            DrawMiddle(SpriteBatch);
            DrawFront(SpriteBatch);
            DrawMostFront(SpriteBatch, gameTime);

            SpriteBatch.End();

            SpriteBatch.Begin();
            DrawUI(SpriteBatch);
            SpriteBatch.End();

            base.Draw(gameTime);

            #region Diagnostics
            s.Stop();
            drawCounter += s.ElapsedMilliseconds;
            drawTicks++;
            #endregion
        }


        #region Draw Methods

        private void DrawBack(SpriteBatch batch)
        {
            EffectManager.DrawBack(batch);
            EntityManager.DrawBack(batch);
        }

        private void DrawMiddle(SpriteBatch batch)
        {
            World.DrawMiddle(batch);
            EffectManager.DrawMiddle(batch);
            EntityManager.DrawMiddle(batch);
        }

        private void DrawFront(SpriteBatch batch)
        {
            EntityManager.DrawFront(batch);
            World.DrawFront(batch);
            EffectManager.DrawFront(batch);

            
        }

        private void DrawMostFront(SpriteBatch batch, GameTime gameTime)
        {
            EffectManager.DrawMostFront(batch);
            World.DrawMostFront(batch);
            EntityManager.DrawMostFront(batch);


            if (Input.MouseSelectionRectangleActive)
            {
                Shapes2D.DrawRectangle(batch, Input.MouseSelectionRectangle, (int)(Camera.ZoomInnerLimit / Camera.Zoom));
            }
        }

        private void DrawUI(SpriteBatch batch)
        {
            NetworkGame.Chat.Draw(batch);
            tools.Draw(batch);
        }


        #endregion

        #endregion

        #region Methods

        protected override BaseDebug GetDebugState()
        {
            return new GamePlayScreenDebug();
        }

        #endregion

    }
}
