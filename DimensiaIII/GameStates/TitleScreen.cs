﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.GameStates
{
    //THIS IS ALSO THE SPLASH SCREEN
    //If you dont know what a splash screen is, get out

    //This should transfer onto the 'menu-screen'
    public class TitleScreen : BaseGameState
    {

        #region Variables

        private float _scale, _alpha;
        //how long the screen should stay ACTIVE.

        #endregion

        #region Constructor

        public TitleScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {
            _scale = _alpha = 1f;
        }

        #endregion

        #region XNA methods

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void ReloadLabels()
        {
            base.ReloadLabels();
        }

        public override void Update(GameTime gameTime)
        {
            if (_alpha < 0.1f || gameTime.TotalGameTime.TotalSeconds > 0) 
            {
                StateManager.ChangeState(game.GameSelectScreen);
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {

            SpriteBatch.Begin();

            SpriteBatch.End();

            base.Draw(gameTime);
        }

        #endregion

        #region Methods

        #endregion

    }
}
