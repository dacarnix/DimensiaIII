﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using DimensiaIII.Tools;
using DimensiaIII.IO;

using DimensiaIII.Networking;
using DimensiaIII.Debug;
using DimensiaIII.Tools.ConcreteTools;
using DimensiaIII.Tools.ConcreteTools.ExpandableTools;
using DimensiaIII.Tools.ConcreteTools.AlignmentOptions;
using DimensiaIII.Tools.ConreteTools.AlignmentOptions;

namespace DimensiaIII.GameStates
{
    //Game selection screen - should always be prior to gameplay screen.
    //sets up variables ready for gameplay

    enum SelectionState
    {
        Menu, //The 'menu' of the game selection screen, ie play/host/join
        Host,
        Join // these just represent each one, as there are intermediate steps for both host and join (ie port/ips etc etc.)
    }

    public class GameSelectionScreen : BaseGameState
    {

        #region Variables

        private SelectionState _currentState;

        private string _ip; //for the connecting IP.
        private int _port; //for the port, these two are for networking purposes.

        #endregion

        #region Constructor

        public GameSelectionScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {
            _currentState = SelectionState.Menu; //Always begins in the menu
            string[] networkingData = MasterIO.LoadNetworking(); //this returns two values, ip and port as a string array
            //IF they are saved previously.This is for 'memory' purposes to save previously entered ips
            if (networkingData != null && networkingData.Length == 2) // safety check
            {
                _ip = networkingData[0]; //ez
                _port = int.Parse(networkingData[1]);
            }
            else
            {
                _ip = ""; _port = 1337; // default values
            }
        }

        #endregion

        #region XNA methods

        protected override void LoadContent()
        {
            base.LoadContent();
            ToolBox toolBox = new ToolBox(new Vector2(0, 60));
            ToolBox innerBox = new ToolBox(new Vector2(150, 0));

            Tool play, host, join;
            play = new Label("Play Single");
            play.Selected += play_Selected;
            host = new Label("Host a game");
            host.Selected += host_Selected;
            join = new Label("Join a game");
            join.Selected += join_Selected;

            TextBox box = new TextBox(TextAlignment.Left);

            toolBox.AddChild(play);
            innerBox.AddChild(host);
            innerBox.AddChild(join);
            toolBox.AddChild(innerBox);
            toolBox.AddChild(box);
            toolBox.Alignment = ToolsAlignment.Centre;
            tools.Add(toolBox);

            toolBox.Position = new Vector2(Main.ScreenWidth / 2, 300);

            ToolBox topLeftBox = new ToolBox(new Vector2(0, 80));
            Tool tB = new Button("Click me!", 200, 50, TextAlignment.Left);
            tB.Alignment = ToolsAlignment.LeftTop;
            ExpandableTool t = new ExpandableTool(tB);
            Tool l = new Label("Memes");
            Tool lm = new Label("Marc");
            Tool sl = new Label("Poos");
            Tool lsm = new Button("!", 30, 30, TextAlignment.Centre);
            Tool lll = new Label("Does this work?");
            ExpandableTool ttt = new ExpandableTool(lsm), tttt = new ExpandableTool(lll);
            ToolBox penis = new ToolBox(new Vector2(0, 40));
            Tool h = new Label("Yes"), hh = new Label("No"), hhh = new Label("Maybe");
            TextBox test = new TextBox(TextAlignment.Centre);
            penis.AddChild(h);
            penis.AddChild(hh);
            penis.AddChild(hhh);
            penis.AddChild(test);
            tttt.AddChild(penis);
            ttt.AddChild(tttt);
            t.AddChild(l);
            t.AddChild(lm);
            t.AddChild(sl);
            t.AddChild(ttt);
            t.SetExpandableRegionAlignment(ToolsAlignment.LeftTop);
            topLeftBox.AddChild(t);
            topLeftBox.Position = new Vector2(20, 300);
            tools.Add(topLeftBox);




            ScrollableTool sb = new ScrollableTool(new Vector2(0, 15), 0, 200, ScrollDirection.UpDown);

            string ss = "a";
            for (int i = 0; i < 6; i++)
            {
                ss += "a";
                Tool tttttt = new Label(ss);
                sb.AddChild(tttttt);
            }
            TextBox tb = new TextBox(TextAlignment.Left);
            tb.Text = ss;
            sb.AddChild(tb);
            for (int i = 0; i < 6; i++)
            {
                ss += "a";
                Tool tttttt = new Label(ss);
                sb.AddChild(tttttt);
            }

            sb.Position = new Vector2(400, 20);

            tools.Add(sb);

        }

        public override void ReloadLabels()
        {
            base.ReloadLabels();
        }

        public override void Update(GameTime gameTime)
        {
            if (_currentState == SelectionState.Join)
            {
                if (Input.KeyPressed(Keys.Space))
                {
                    JoinScreen(); //refresh it
                } 
                else
                {

                    foreach (DimensiaIII.Networking.Browser.ServerData sd in NetworkGame.Browser.Data)
                    {
                        if (sd.HasClick)
                        {
                            NetworkGame.AttemptConnection(new System.Net.IPEndPoint(System.Net.IPAddress.Parse(sd.IP), sd.Port));
                        }
                    }

                    if (NetworkGame.ConnectedToAGame)
                    {
                        StateManager.ChangeState(game.GamePlayScreen);
                        return;
                    }

                    NetworkGame.Update(gameTime);
                }
            }

            if (game.IsActive)
            {

            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch.Begin();
            if (_currentState == SelectionState.Menu) tools.Draw(SpriteBatch);

            if (_currentState == SelectionState.Join)
            {
                NetworkGame.Browser.Draw(SpriteBatch);
            }

            SpriteBatch.End();

            base.Draw(gameTime);
        }

        #endregion

        #region Methods

        protected override BaseDebug GetDebugState()
        {
            return new GameSelectionDebug();
        }

        #endregion

        #region Events

        void join_Selected(object sender, EventArgs e)
        {
            _currentState = SelectionState.Join;
            NetworkGame.SetUpNetworking(false, false, null, 12424);

            //StateManager.ChangeState(game.GamePlayScreen);
        }

        void host_Selected(object sender, EventArgs e)
        {
            _currentState = SelectionState.Host;
            NetworkGame.SetUpNetworking(true, false, null, 12424);
            StateManager.ChangeState(game.GamePlayScreen);

        }

        void JoinScreen()
        {
            _currentState = SelectionState.Join;
            NetworkGame.SetUpNetworking(false, false, null, 12424);
        }

        void play_Selected(object sender, EventArgs e)
        {
            NetworkGame.SetUpNetworking(false, false, _ip, _port);
            StateManager.ChangeState(game.GamePlayScreen);
        }

        #endregion

    }
}
