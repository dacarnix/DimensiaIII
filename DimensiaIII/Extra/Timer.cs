﻿using Microsoft.Xna.Framework;

namespace DimensiaIII.Extra
{
    public class Timer : GameComponent
    {
        #region Variables and Properties
       
        #endregion

        #region Constructor

        public Timer(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        #endregion

        #region XNA Methods

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here


            base.Update(gameTime);
        }

        #endregion

        #region Methods

        #endregion
    }
}
