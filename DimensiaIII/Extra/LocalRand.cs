﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

namespace DimensiaIII.Extra
{
    public class LocalRand
    {

        private Random rand;

        public void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public LocalRand()
        {
            rand = new Random();
        }

        public LocalRand(int seed)
        {
            rand = new Random(seed);
        }

            public int GetInt(int top)
            {
                return rand.Next(top);
            }
            public int GetInt(int low, int top)
            {
                return rand.Next(low, top);
            }

            public float GetFloat(int low, int top)
            {
                return (float)rand.NextDouble() * (rand.Next(low, top));
            }

            public double GetDouble(int low, int top)
            {
                return rand.NextDouble() * (rand.Next(low, top));
            }

            public double GetDouble(double low, double top)
            {
                return rand.NextDouble() * (top - low) + low;
            }

            public float GetFloat(float low, float top)
            {
                return (float)(rand.NextDouble() * (top - low) + low);
            }

            public float GetFloat(double low, double top)
            {
                return (float)(rand.NextDouble() * (top - low) + low);
            }

            public Color GetColor()
            {
                return new Color(rand.Next(255), rand.Next(255), rand.Next(255));
            }

            public Color GetSimilarColor(Color c)
            {
                return new Color(rand.Next(Math.Max(c.R - 30, 0), Math.Min(255, c.R + 30)), rand.Next(Math.Max(c.G - 30, 0), Math.Min(255, c.G + 30)), rand.Next(Math.Max(c.B - 30, 0), Math.Min(255, c.B + 30)));
            }

            public Color GetSimilarColor(Color c, int difference)
            {
                return new Color(rand.Next(Math.Max(c.R - difference, 0), Math.Min(255, c.R + difference)), rand.Next(Math.Max(c.G - difference, 0), Math.Min(255, c.G + difference)),
                    rand.Next(Math.Max(c.B - difference, 0), Math.Min(255, c.B + difference)));
            }

    }
}
