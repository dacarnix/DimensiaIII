﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using DimensiaIII.Entities.NPCS;

namespace DimensiaIII.Entities
{
    public static class EntityManager 
    {
        #region Variables and Properties

        public const int MaxLocalPlayers = 1; //One now, silly to have multiple
        public const int MaxOnlinePlayers = 7; // Can have up to 7 others (ie 8 people max), however this should be
        //flexible to allow more if needed later -- NO HARD CODING

        private static Entity[] _players;

        private static LinkedList<Entity> _npcs;

        private static Dictionary<string, int> _networkPlayers; // this is to assign a unique ip to a player id.
        private static int _numberConnected;
        private static List<Player> _extraPlayers; //this can either be AI or 
        private static Dictionary<string, int> _onlineIdentifier; //maps an IP to an index in the player list, ez.

        private static bool _rectangleMaking; //for tracking whether a rectangle should be being formed

        public static string PlayerName { get { return _players[0].Name; } }
        public static Entity MainPlayer { get { return _players[0]; } }

        public static int NPCCounter { get { return _npcs.Count; } }


      
        #endregion

        #region Set up

        public static void SetUp()
        {

            _players = new Entity[MaxLocalPlayers];
            _npcs = new LinkedList<Entity>();
            _onlineIdentifier = new Dictionary<string, int>();

            for (int i = 0; i < MaxLocalPlayers; i++)
            {
                _players[i] = new Player(i);
            }

            _players[0].OnConnect(); //should always be atleast one player connected

            _numberConnected = 0;
            _networkPlayers = new Dictionary<string, int>();
            _extraPlayers = new List<Player>();

        }

        #endregion
        
        #region XNA Methods

        public static void Update(GameTime gameTime)
        {

            #region Entity updates

            for (int i = 0; i < MaxLocalPlayers; i++)
            {
                if (_players[i].IsActive && !_players[i].Update(gameTime)) //make sure is active
                { 
                    _players[i].OnRemove(); // :(
                }
            }

            for (int i = 0; i < _extraPlayers.Count; i++)
            {
                if (!_extraPlayers[i].Update(gameTime)) //make sure is active
                {
                    _extraPlayers[i].OnRemove(); // :(
                }
            }

            var currentNode = _npcs.First;
            while (currentNode != null)
            {
                if (!currentNode.Value.Update(gameTime))
                {
                    currentNode.Value.OnRemove(); // call death routine
                    var toRemove = currentNode;
                    currentNode = currentNode.Next;
                    _npcs.Remove(toRemove);
                }
                else
                {
                    currentNode = currentNode.Next;
                }
            }

            #endregion


        }

        #region Draw

        public static void DrawBack(SpriteBatch batch)
        {
            foreach (Entity player in _players)
                if (player.IsActive) player.DrawBack(batch);

            foreach (Entity player in _extraPlayers)
                if (player.IsActive) player.DrawBack(batch);

            foreach (Entity npc in _npcs)
                if (npc.IsActive) npc.DrawBack(batch);  
        }

        public static void DrawMiddle(SpriteBatch batch)
        {
            foreach (Entity player in _players)
                if (player.IsActive) player.DrawMiddle(batch);

            foreach (Entity player in _extraPlayers)
                if (player.IsActive) player.DrawMiddle(batch);

            foreach (Entity npc in _npcs)
                if (npc.IsActive) npc.DrawMiddle(batch);  
        }

        public static void DrawFront(SpriteBatch batch)
        {
            foreach (Entity player in _players)
                if (player.IsActive) player.DrawFront(batch);

            foreach (Entity player in _extraPlayers)
                if (player.IsActive) player.DrawFront(batch);

            foreach (Entity npc in _npcs)
                if (npc.IsActive) npc.DrawFront(batch);  
        }

        public static void DrawMostFront(SpriteBatch batch)
        {
            foreach (Entity player in _players)
                if (player.IsActive) player.DrawMostFront(batch);

            foreach (Entity player in _extraPlayers)
                if (player.IsActive) player.DrawMostFront(batch);

            foreach (Entity npc in _npcs)
                if (npc.IsActive) npc.DrawMostFront(batch);

        }

        #endregion
  
        

        #endregion

        #region Methods
        //INPUT.cs now detected new players connected/dropped
        //VIA CONTROLLERS! (important)

        #region Online and AI Players

        public static void AddAIPlayer()
        {

        }

        public static void UpdateOnlinePosition(string ip, Vector2 position)
        {
            _extraPlayers[_onlineIdentifier[ip]].Position = position;
        }

        public static void AddPlayer(bool ai, bool online, Vector2 position)
        {

        }

        public static void AddOnlinePlayer(string ip)
        {
            Player p = new Player(10);
            p.IsOnline = true;
            p.OnConnect();
            _onlineIdentifier.Add(ip, _extraPlayers.Count);

            _extraPlayers.Add(p);
        }

        #endregion

        #region NPCs

        public static void AddNPC(NPCType npcType, int owner)
        {
            AddNPC(_players[0].Position, npcType, owner);
        }

        public static void AddNPC(Vector2 position, NPCType npcType, int owner)
        {
            AddNPC(position, npcType, AIType.Default, owner);
        }

        public static void AddNPC(Vector2 position, NPCType npcType, AIType ai, int owner)
        {
            _npcs.AddLast(NPCFactory.CreateNPC(position, npcType, ai));
        }

        #endregion

        #endregion

    }
}
