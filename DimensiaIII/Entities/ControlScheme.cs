﻿using System;
using Microsoft.Xna.Framework.Input;
using DimensiaIII.IO;

namespace DimensiaIII.Entities
{

    enum ControlNum
    {
        Left = 0,
        Right = 1,
        Up = 2,
        Down = 3,
    }

    public sealed class ControlScheme
    {

        #region Variables and Properties

        private static readonly int NumberOfControls = Enum.GetNames(typeof(ControlNum)).Length;
        private Keys[] _controls;
        //CONTROLLER SUPPORT IS NO LONGER DEVELOPED
        //ditched this as RTS is too complicated for controllers

        //Unique keys in here
        #region Properties

        public Keys Left { get { return _controls[(int)ControlNum.Left]; } }
        public Keys Right { get { return _controls[(int)ControlNum.Right]; } }
        public Keys Up { get { return _controls[(int)ControlNum.Up]; } }
        public Keys Down { get { return _controls[(int)ControlNum.Down]; } }

        #endregion

        #endregion

        #region Constructor

        public ControlScheme()
        {
            _controls = ControlScheme.PlayerDefault();

            if (MasterIO.HasPlayerControls()) //There are some saved controls
            {
                _controls = MasterIO.LoadControls();
            }

        }

        #endregion

        #region XNA Methods

        #endregion

        #region Static Defaults

        private static Keys[] PlayerDefault()
        {
            Keys[] keys = new Keys[NumberOfControls];

                keys[(int)ControlNum.Left] = Keys.A;
                keys[(int)ControlNum.Right] = Keys.D;
                keys[(int)ControlNum.Up] = Keys.W;
                keys[(int)ControlNum.Down] = Keys.S;
            
            return keys;
        }

        public static string[] KeyNames()
        {

            string[] names = new string[NumberOfControls];

            foreach (ControlNum day in Enum.GetValues(typeof(ControlNum)))
            {
                names[(int)day] = day.ToString().Replace('_', ' ');
            }

            return names;
        }

        #endregion

        #region Methods


        #endregion

    }
}
