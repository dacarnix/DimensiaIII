﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Entities
{
    public abstract class Entity
    {

        #region Variables and Properties

        protected Vector2 _pos, _vel, _oldPos, _oldVel;

        protected bool _forceDeath, _isActive, _inputConnected, _disconnected;
        protected bool _visible, _invincible, _flashing;
        protected Vector2 _accelerationX, _accelerationY;
        protected float _yVelCap, _xVelCap;
        protected bool _isNetworkPlayer;

        protected bool _lockedToCamera;
        protected Vector2 _targetPosition;
        protected bool _isAutoPathing; //this and the vector above is for the autopathing algorithm

        protected byte _tileHeight;

        protected int _id;

        protected float _alpha;

        protected string _name;

        protected Color _color;

        protected int _width, _height;
        protected Vector2 _dimensions; //just a vector representation 

        protected Rectangle _basicCollision; //bounds of a entity for unit selection

        protected bool _selected;

        public Rectangle BasicCollision { get { return _basicCollision; } }
        public bool Selected { get { return _selected; } }
        public string Name { get { return _name; } }
        public Vector2 Position { get { return _pos; } set { _pos = value; } }
        public Vector2 Velocity { get { return _vel; } }

        public int Width { get { return _width; } }
        public int Height { get { return _height; } }

        public bool IsOnline
        {
            get { return _isNetworkPlayer; }
            set { _isNetworkPlayer = value; }
        }

        public bool LockToCamera { get { return _lockedToCamera; } }

        public bool Disconnected {  get { return _disconnected; } }

        public bool IsActive { get { return _isActive; } }
        public bool InputConnected { get { return _inputConnected; } }

        public Vector2 CentrePosition { get { return _pos + (_dimensions / 2); } }

        public byte TileHeight { get { return _tileHeight; } }

        #endregion

        #region Constructor

        public Entity()
        {
            _color = Color.White;
            _vel = Vector2.Zero;
            _alpha = 1f;
            _isNetworkPlayer = false;
            _xVelCap = _yVelCap = 10f;
            _basicCollision = new Rectangle();
            _selected = false;
            _lockedToCamera = false;
            _tileHeight = 0;
        }

        #endregion

        #region XNA Methods

        public abstract bool Update(GameTime gameTime);

        #region Draw

        public virtual void DrawBack(SpriteBatch batch)
        {

        }

        public virtual void DrawMiddle(SpriteBatch batch)
        {

        }

        public virtual void DrawFront(SpriteBatch batch)
        {

        }

        public virtual void DrawMostFront(SpriteBatch batch)
        {

        }

        public void DrawAll(SpriteBatch batch)
        {
            DrawBack(batch);
            DrawMiddle(batch);
            DrawFront(batch);
            DrawMostFront(batch);
        }

        #endregion   

        #endregion

        #region Methods

        public virtual void SetTargetPosition(Vector2 value)
        {
            _targetPosition = value;
            _isAutoPathing = true;
        }

        public virtual void SetSelected(bool value)
        {
            _selected = value;
        }

        public virtual void OnSpawn()
        {

        }

        public virtual void OnRemove()
        {

        }

        #region Controller/Input

        public virtual void OnDisconnect()
        {

        }

        public virtual void OnConnect()
        {

        }

        #endregion

        #endregion

    }
}
