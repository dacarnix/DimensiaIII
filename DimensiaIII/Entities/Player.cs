﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using DimensiaIII.Effects.Particles;
using DimensiaIII.WorldEngine;

namespace DimensiaIII.Entities
{
    public class Player : Entity
    {
        #region Variables and Properties

        private readonly int _playerID;
        private ControlScheme _controls;

        public int PlayerID { get { return _playerID; } }
        public ControlScheme Controls { get { return _controls; } }

        #endregion

        #region Constructor

        public Player(int id) : base()
        {
            _playerID = id;
            _pos = Vector2.Zero;
            _controls = new ControlScheme();
            _accelerationX = new Vector2(0.5f,0); //default values?
            _accelerationY = new Vector2(0, 0.5f);
            _width = _height = 80;
            _dimensions = new Vector2(_width, _height);
            _xVelCap = _yVelCap = 40;
            //change this later
        }

        #endregion

        #region XNA Methods

        public override bool Update(GameTime gameTime)
        {

            _oldPos = _pos;
            _oldVel = _vel; //Stores values before updating the new

            _basicCollision.Width = _width;
            _basicCollision.Height = _height;
            _basicCollision.X = (int)_pos.X;
            _basicCollision.Y = (int)_pos.Y;

            #region Controls and Movement

            if (!_isNetworkPlayer)
            {
                
                if (Input.KeyDown(_controls.Left))
                {
                    _vel -= _accelerationX;
                }

                if (Input.KeyDown(_controls.Right))
                {
                    _vel += _accelerationX;
                }

                if (Input.KeyDown(_controls.Up))
                {
                    _vel -= _accelerationY;
                }

                if (Input.KeyDown(_controls.Down))
                {
                    _vel += _accelerationY;
                }


                if (Main.DEBUG)
                {
                    if (Input.KeyDown(Keys.U))
                    {
                        _accelerationX = new Vector2(2f, 0); //default values?
                        _accelerationY = new Vector2(0, 2f);
                        Effects.EffectManager.AddParticles(ParticleType.Explosion, this.CentrePosition, 10,
                            Rand.GetColor(), Rand.GetInt(600, 1000));
                        Camera.LockToEntity(this);
                        _vel.X = MathHelper.Clamp(_vel.X, -_xVelCap * 2, _xVelCap * 2);
                        _vel.Y = MathHelper.Clamp(_vel.Y, -_yVelCap * 2, _yVelCap * 2);
                    }
                    else
                    {

                        _accelerationX = new Vector2(0.5f, 0); //default values?
                        _accelerationY = new Vector2(0, 0.5f);

                        _vel.X = MathHelper.Clamp(_vel.X, -_xVelCap, _xVelCap);
                        _vel.Y = MathHelper.Clamp(_vel.Y, -_yVelCap, _yVelCap);
                    }
                }

                _pos += _vel;

            }

            #endregion

            if (Input.KeyPressed(Keys.U))
            {
                _lockedToCamera = (_lockedToCamera) ? false : true;
            }


            if (_lockedToCamera) Camera.LockToEntity(this); //test

            return true;
        }


        public override void DrawFront(SpriteBatch batch)
        {
            batch.Draw(Pipeline.TextureManager.Pixel, MathAid.Vector2ToRectangle(_pos, _width, _height), _color);
        }

        #endregion

        #region Methods

        public override void OnConnect()
        {
            _isActive = true;
            _disconnected = false;
            base.OnConnect();
        }

        public override void OnDisconnect()
        {
            _isActive = false;
            _disconnected = true;
            base.OnDisconnect();
        }

        #endregion

    }
}
