﻿using Microsoft.Xna.Framework;

namespace DimensiaIII.Entities.NPCS
{
    public class NPC : Entity
    {
        #region Variables and Properties

        private AIType _ai;

        #endregion

        #region Constructor

        public NPC(Vector2 position, AIType ai)
        {
            _ai = ai;
            _pos = position;
        }

        #endregion

        #region XNA Methods

        public override bool Update(GameTime gameTime)
        {

            _basicCollision.Width = _width;
            _basicCollision.Height = _height;
            _basicCollision.X = (int)_pos.X;
            _basicCollision.Y = (int)_pos.Y;

            _vel.X = MathHelper.Clamp(_vel.X, -_xVelCap, _xVelCap);
            _vel.Y = MathHelper.Clamp(_vel.Y, -_yVelCap, _yVelCap);

            _pos += _vel;

            return !_forceDeath;

        }

        #endregion

        #region Methods

        #endregion

    }
}
