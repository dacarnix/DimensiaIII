﻿using System;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DimensiaIII.Extra;
using DimensiaIII.Pipeline;
using DimensiaIII.GameStates;
using DimensiaIII.Effects;
using DimensiaIII.Entities;
using DimensiaIII.WorldEngine;
using DimensiaIII.WorldEngine.TileEngine;

namespace DimensiaIII
{
    /// <summary>
    /// This is the main type for your game.
    /// Uses FARSEER
    /// https://www.nuget.org/packages/FarseerPhysics.MonoGame.Unofficial/3.5.0
    /// </summary>
    /// 

    #region Small Global Enums

    public enum GfxSettings
    {
        Low = 10,
        Medium = 15,
        High = 20
    }

    #endregion

    public class Main : Game
    {

        #region Variables and Properties

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public static bool Screenshot = false;
        private static int _sWidth = 800, _sHeight = 500;
        private static Rectangle _screenRect;
        private static Vector2 _centreScreen = Vector2.Zero;
        private static int monitorWidth, monitorHeight;

        public static string GameName { get { return "DimensiaIII"; } }
        public static float GameVersion { get { return 0.2f; } }

        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        public static Vector2 CentreScreenFullResolution
        {
            get { return new Vector2(Main.MonitorWidth / 2, Main.MonitorHeight / 2); }
        }

        public static int MonitorWidth
        {
            get { return monitorWidth; }
        }

        public static int MonitorHeight
        {
            get { return monitorHeight; }
        }

        public GraphicsDeviceManager GraphicsManager
        {
            get { return graphics; }
        }

        public static Rectangle ScreenRectangle
        {
            get { return _screenRect; }
        }

        public static Vector2 CentreScreen { get { return _centreScreen; } }

        public static int ScreenWidth { get { return _sWidth; } }
        public static int ScreenHeight { get { return _sHeight; } }

        public static bool DEBUG = false;

        public static bool DEVELOPMENT = true;

        #region Game States

        private GameStateManager stateManager;
        private TitleScreen _titleScreen;
        private MenuScreen _menuScreen;
        private GameSelectionScreen _gameSelect;
        private GamePlayScreen _gameScreen;

        #region Respective Properties

        public GameStateManager StateManager { get { return stateManager; } }
        
        public TitleScreen TitleScreen { get { return _titleScreen; } }
        public MenuScreen MenuScreen { get { return _menuScreen; } }
        public GameSelectionScreen GameSelectScreen { get { return _gameSelect; } }
        public GamePlayScreen GamePlayScreen { get { return _gameScreen; } }

        #endregion

        #endregion

        #endregion

        #region Constructor

        public Main()
        {

            #region New System Compontents

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            Components.Add(new Input(this));
            Components.Add(new Timer(this));

            stateManager = new GameStateManager(this);
            Components.Add(stateManager);

            _titleScreen = new TitleScreen(this, stateManager);
            _menuScreen = new MenuScreen(this, stateManager);
            _gameSelect = new GameSelectionScreen(this, stateManager);
            _gameScreen = new GamePlayScreen(this, stateManager);

            stateManager.ChangeState(_titleScreen);

            #endregion

            #region Settings (Saves etc)

            graphics.SynchronizeWithVerticalRetrace = false;
            graphics.ApplyChanges();

            #endregion

        }

        #endregion

        #region XNA Methods

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            TextureManager.Initilize();
            AudioManager.Initilize();
            FontManager.Initilize();
            IO.MasterIO.SetUp();
            Engine.Initilize(32, 32); //Set the tile dimensions here
            EffectManager.SetUp();
            EntityManager.SetUp();
            Camera.Refresh(GraphicsDevice.Viewport, Main.ScreenWidth, Main.ScreenHeight);

            this.Window.AllowUserResizing = true;

            _screenRect = new Rectangle(0, 0, _sWidth, _sHeight);

            Shapes2D.SetUp(this);

            base.Initialize();
            monitorHeight = GraphicsDevice.DisplayMode.Height;
            monitorWidth = GraphicsDevice.DisplayMode.Width;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            TextureManager.LoadContent(Content, GraphicsDevice);
            AudioManager.LoadContent(Content);
            FontManager.LoadContent(Content);
            World.LoadGameWorld();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            this.spriteBatch.Dispose();
            this.Content.Unload();
            this.Content.Dispose();
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //Leave Resolution check here, always
            ResolutionCheck(); //Required for MonoGame (piece of sh*t) XNA >>>> MG

            _centreScreen.X = Main.ScreenWidth / 2;
            _centreScreen.Y = Main.ScreenHeight / 2;

            ScreenShotUpdate(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);    

            base.Draw(gameTime);
        }

        #endregion

        #region Events

        private void ResolutionCheck()
        {
            // Make changes to handle the new window size.
            int newWidth = Window.ClientBounds.Width;
            int newHeight = Window.ClientBounds.Height;


            if (newWidth != _sWidth || newHeight != _sHeight)
            {
                if (newWidth < 800) newWidth = 800;
                if (newHeight < 500) newHeight = 500; //silly dimensions
                graphics.PreferredBackBufferHeight = newHeight;
                graphics.PreferredBackBufferWidth = newWidth;
                graphics.ApplyChanges();
                _sHeight = newHeight;
                _sWidth = newWidth; //Fancy code -- don't touch
                _screenRect.Width = _sWidth;
                _screenRect.Height = _sHeight;

                Camera.Refresh(GraphicsDevice.Viewport, Main.ScreenWidth, Main.ScreenHeight);

                stateManager.CurrentState.ReloadLabels();
            }
 
        }

        #endregion

        #region Screen Shot code

        private void ScreenShotUpdate(GameTime gameTime)
        {

            if (Input.KeyPressed(IO.MasterIO.ScreenshotKey)) //Button for screenshot
            {

                bool success = true;
                try
                {
                    TakeDump(gameTime); //Take a screenshot
                }
                catch
                {
                    success = false;
                }
                finally
                {
                    if (DEBUG)
                    {
                        stateManager.CurrentState.PulseScreenshot(success);
                    }
                }
            }

        }

        private void TakeDump(GameTime gameTime)
        {

            DateTime dt = DateTime.Now;

            string month;
            if (dt.Month.ToString().Length == 1)
            {
                month = "0" + dt.Month.ToString();
            }
            else month = dt.Month.ToString();

            string day;
            if (dt.Day.ToString().Length == 1)
            {
                day = "0" + dt.Day.ToString();
            }
            else day = dt.Day.ToString();

            string hour;
            if (dt.Hour.ToString().Length == 1)
            {
                hour = "0" + dt.Hour.ToString();
            }
            else hour = dt.Hour.ToString();

            string second;
            if (dt.Second.ToString().Length == 1)
            {
                second = "0" + dt.Second.ToString();
            }
            else second = dt.Second.ToString();


            string fileName = month + "-" + day + "-" + hour + "-" + second + ".png";

            string path = IO.MasterIO.ScreenshotPath;

            if (graphics.IsFullScreen)
            {

                RenderTarget2D screenshot = new RenderTarget2D(GraphicsDevice, Main.ScreenWidth, Main.ScreenHeight);

                GraphicsDevice.SetRenderTarget(screenshot);

                stateManager.CurrentState.Draw(gameTime);

                GraphicsDevice.SetRenderTarget(null);


                using (FileStream fs = new FileStream(path + Path.DirectorySeparatorChar + fileName, FileMode.OpenOrCreate))
                {
                    screenshot.SaveAsPng(fs, Main.ScreenWidth, Main.ScreenHeight);

                    fs.Dispose();

                }


            }
            else
            {
                System.Drawing.Bitmap b = new System.Drawing.Bitmap(Main.ScreenWidth, Main.ScreenHeight);

                System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(b);
                g.CopyFromScreen(Window.ClientBounds.X, Window.ClientBounds.Y, 0, 0, new System.Drawing.Size(Main.ScreenWidth, Main.ScreenHeight));

                b.Save(path + Path.DirectorySeparatorChar + fileName);
            }
        }


        #endregion

    }
}
