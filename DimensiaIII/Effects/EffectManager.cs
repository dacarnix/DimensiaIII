﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using DimensiaIII.Effects.Settings;
using DimensiaIII.Effects.Particles;

namespace DimensiaIII.Effects
{
    /// <summary>
    /// EffectManager is essentially a SINGLETON
    /// It will handle all effect based logic/draws.
    /// 
    /// This includes the particle manager, particles will be added THROUGH the EffectManager,
    /// and therefore also doubles as a Factory pattern class.
    /// </summary>
    public static class EffectManager
    {

        #region Variables and Properties
        private static ParticleSettings _particleSetting;
        private static GraphicSettings _graphicSettings;

        private static ParticleManager _particleManager;

        private static ParticleManager GetParticleManager
        {
            get
            {
                if (_particleManager == null) _particleManager = new ParticleManager();
                return _particleManager;
            }
        }

        public static int ParticleCount {  get { return GetParticleManager.ParticleCount; } }
        
        #endregion

        #region Set up
        public static void SetUp()
        {
            
        }

        #endregion

        #region XNA Methods

        public static void Update(GameTime gameTime)
        {
            GetParticleManager.Update(gameTime);
        }

        #region Draw

        public static void DrawBack(SpriteBatch batch)
        {
            GetParticleManager.DrawBack(batch);
        }

        public static void DrawMiddle(SpriteBatch batch)
        {
            GetParticleManager.DrawMiddle(batch);
        }

        public static void DrawFront(SpriteBatch batch)
        {
            GetParticleManager.DrawFront(batch);
        }

        public static void DrawMostFront(SpriteBatch batch)
        {
            GetParticleManager.DrawMostFront(batch);
        }

        public static void DrawAll(SpriteBatch batch)
        {
            GetParticleManager.DrawAll(batch);
        }

        #endregion   

        #endregion

        #region Methods

        public static void SetParticleSetting(ParticleSettings ps) { _particleSetting = ps; }
        public static void SetGraphicSetting(GraphicSettings gs) { _graphicSettings = gs; }

        #region Particles

        /// <summary>
        /// An F tonne of overloads
        /// </summary>
        /// <param name="pointTo"></param>
        /// <param name="num"></param>

        public static void AddParticles(ParticleType type, Vector2 position, int amount)
        {
            AddParticles(type, position, amount, Color.White);
        }

        public static void AddParticles(ParticleType type, Vector2 position, int amount, Color color)
        {
            AddParticles(type, position, amount, color, 1000);
        }

        public static void AddParticles(ParticleType type, Vector2 position, int num, Color color, int lifeSpan, int explosionRadius = 80)
        {
            //Stopwatch sw = new Stopwatch();
            //sw.Start();

            GetParticleManager.AddParticle(position, num, type, color, explosionRadius, lifeSpan, EffectDepth.Middle);

            //sw.Stop();
            //System.Windows.Forms.MessageBox.Show(sw.Elapsed.ToString());
        }

        #endregion

        public static void ClearParticles()
        {
            GetParticleManager.Flush(); //empties all particles, but NOT asteroids
        }

        #endregion

    }
}
