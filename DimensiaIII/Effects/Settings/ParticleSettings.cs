﻿namespace DimensiaIII.Effects.Settings
{
    public enum ParticleSettings
    {
        Low = 200,
        Normal = 400,
        High = 600,
        Uncapped = 1000
    }
}
