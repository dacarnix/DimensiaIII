﻿namespace DimensiaIII.Effects.Settings
{
    public enum EffectDepth
    {
        Back,
        Middle,
        Front,
        MostFront
    }
}
