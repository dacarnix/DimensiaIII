﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Effects
{
    /// <summary>
    /// The base class to all effects (including particles).
    /// 
    /// Defines the base common methods to be used amongst all
    /// </summary>
    public abstract class Effect
    {

        #region Base Variables and Properties

        protected Vector2 _pos, _vel, _orgPos;

        //The color TINT of an effect (Color.White gives no tint)
        protected Color _color;

        //Opacity, 1 = normal, < 1 = more transparaency.
        protected float _alpha;

        protected float _scale;

        public Vector2 Position { get { return _pos; } }

        #endregion


        #region Abstract methods

        public abstract bool Update(GameTime gameTime);
        public abstract void Draw(SpriteBatch batch);

        #endregion

        #region Virtual Methods

        /// <summary>
        /// A useful method to define any special identifying variables
        /// About a subclass.
        /// </summary>
        public virtual void MakeUnique() { }

        /// <summary>
        /// Can be called upon being removed, essentially.
        /// Blank unless overriden
        /// </summary>
        public virtual void OnRemove() { }

        #endregion

    }
}
