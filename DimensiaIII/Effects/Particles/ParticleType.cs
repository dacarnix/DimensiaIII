﻿using Microsoft.Xna.Framework;
using DimensiaIII.Effects.Particles.ParticleTypes;

namespace DimensiaIII.Effects.Particles
{
    public enum ParticleType
    {
        Default,
        MouseFollow,
        Explosion
    }

    /// <summary>
    /// An extension to the above enum
    /// </summary>
    static class ParticleTypeMethods
    {
        /// <summary>
        /// Maps a particletype to a particle
        /// </summary>
        /// <returns></returns>
        public static Particle GetParticle(this ParticleType type, Vector2 pos, Color c, int lifeSpan, int explosionRadius)
        {
            switch (type)
            {
                case ParticleType.Default:
                    return new ExplosiveParticle(pos, c, explosionRadius, lifeSpan);
                case ParticleType.MouseFollow:
                    return new MouseParticle(pos, c);
                case ParticleType.Explosion:
                    return new ExplosiveParticle(pos, c, explosionRadius, lifeSpan);
                default:
                    return new ExplosiveParticle(pos, c, explosionRadius, lifeSpan);
            }
        }
    }
}