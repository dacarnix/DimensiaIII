﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DimensiaIII.Effects.Settings;

namespace DimensiaIII.Effects.Particles
{
    /// <summary>
    /// This is a pretty hardcore class
    /// Handles all interactions of particles, of all game layers (back,middle,front, etc).
    /// uses LINKED LISTS as this is the most efficient to traverse and delete from (in the centre etc).
    /// 
    /// Actually maybe a set/hashmap .. ?
    /// </summary>
    public class ParticleManager
    {

        #region Variables and Properties

        private LinkedList<Particle> _back, _middle, _front, _mFront;

        public int ParticleCount
        {
            get { return _back.Count + _middle.Count + _front.Count + _mFront.Count; }
        }

        #endregion

        #region Initilization

        public ParticleManager()
        {
            _back = new LinkedList<Particle>();
            _middle = new LinkedList<Particle>();
            _front = new LinkedList<Particle>();
            _mFront = new LinkedList<Particle>();

        }

        #endregion

        #region XNA Methods

        public void Update(GameTime gameTime)
        {
            UpdateList(_back, gameTime);
            UpdateList(_middle, gameTime);
            UpdateList(_front, gameTime);
            UpdateList(_mFront, gameTime);
        }

        private void UpdateList(LinkedList<Particle> list, GameTime gameTime)
        {
            var currentNode = list.First;
            while (currentNode != null)
            {
                if (!currentNode.Value.Update(gameTime))
                {
                    currentNode.Value.OnRemove(); // call death routine
                    var toRemove = currentNode;
                    currentNode = currentNode.Next;
                    list.Remove(toRemove);
                }
                else
                {
                    currentNode = currentNode.Next;
                }
            }

        }

        public void DrawBack(SpriteBatch batch)
        {
            foreach (Particle p in _back)
                p.Draw(batch);
        }

        public void DrawMiddle(SpriteBatch batch)
        {
            foreach (Particle p in _middle)
                p.Draw(batch);

        }

        public void DrawFront(SpriteBatch batch)
        {
            foreach (Particle p in _front)
                p.Draw(batch);
        }

        public void DrawMostFront(SpriteBatch batch)
        {
            foreach (Particle p in _mFront)
                p.Draw(batch);
        }

        public void DrawAll(SpriteBatch batch)
        {
            DrawBack(batch);
            DrawMiddle(batch);
            DrawFront(batch);
            DrawMostFront(batch);
        }

        #endregion

        #region Methods

        #region Adding Particles
        /// <summary>
        /// Adds particles to the particle engine!
        /// </summary>
        /// <param name="position"></param>
        /// <param name="vel"></param>
        /// <param name="num"> The amount of particles to generate </param>
        /// <param name="type"></param>
        /// <param name="color"></param>
        /// <param name="explosionRadius"></param>
        /// <param name="depth"></param>
        public void AddParticle(Vector2 position, int num, ParticleType type, Color color, int explosionRadius, int lifeSpan, EffectDepth depth)
        {
            for (int i = 0; i < num; i++)
            {
                AddParticle(type.GetParticle(position, color, lifeSpan, explosionRadius), depth);
            } 
        }

        private void AddParticle(Particle p, EffectDepth depth)
        {
            switch (depth)
            {
                case EffectDepth.Back:
                    {
                        _back.AddFirst(p);
                        break;
                    }

                case EffectDepth.Middle:
                    {
                        _middle.AddFirst(p);
                        break;
                    }
                case EffectDepth.Front:
                    {
                        _front.AddFirst(p);
                        break;
                    }
                case EffectDepth.MostFront:
                    {
                        _mFront.AddFirst(p);
                        break;
                    }
            }
        }

        #endregion

        #region Cleaning Up Methods

        public void Flush() 
        {
            FlushList(_back);
            FlushList(_middle);
            FlushList(_front);
            FlushList(_mFront);
        }


        private void FlushList(LinkedList<Particle> list)
        {

            list.Clear();
        }

        #endregion

        #endregion

    }
}
