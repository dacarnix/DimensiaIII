﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Effects.Particles.ParticleTypes
{
    public class MouseParticle : Particle
    {

        #region Variables and Properties

        #endregion

        #region Constructor

        public MouseParticle(Vector2 pos, Color c) 
        {
            Regenerate(pos, c);
        }

        #endregion

        #region XNA Methods

        public override bool Update(GameTime gameTime)
        {
            return base.Update(gameTime);
        }

        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(Pipeline.TextureManager.Pixel, _pos, null, _color * _alpha, _rotation, new Vector2(0.5f, 0.5f), _scale, SpriteEffects.None, 1f);
        }

        #endregion

        #region Methods

        public override void MakeUnique()
        {
            _vel = new Vector2(Rand.GetFloat(-2, 2), Rand.GetFloat(-2, 2));
            _vel += -Input.MouseVelocity / 4;
            _scale = Rand.GetFloat(1, 4);
            _alphaDecay = 0.01f;
        }

        public override void OnRemove()
        {
            //EffectManager.ParticleExplode(_pos, 1);
            base.OnRemove();
        }

        #endregion


    }
}
