﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Effects.Particles.ParticleTypes
{

    public class ExplosiveParticle : Particle
    {

        //radius you want to explore around the centre
        protected int _explosionRadius;

        public ExplosiveParticle(Vector2 pos, Color c, int explosionRadius, int lifeSpan)
        {
            _explosionRadius = explosionRadius;
            _lifeSpan = lifeSpan;
            Regenerate(pos, c);
        }


        public override bool Update(GameTime game)
        { 
            return base.Update(game);
        }

        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(Pipeline.TextureManager.Pixel, _pos, null, _color * _alpha, 0f, new Vector2(0.5f,0.5f), _scale, SpriteEffects.None, 1f);
        }

        public override void MakeUnique()
        {
            if (_lifeSpan == INFINITE_LIFE) _lifeSpan = 800;
            _scale = 1.5f;
            _alphaDecay = 15f / (float)(_lifeSpan);
            Vector2 displacement = new Vector2(_explosionRadius / 10.0f, _explosionRadius / 10.0f) / 3;
            float angle = MathHelper.ToRadians(Rand.GetInt(0,360));
            displacement = Vector2.Transform(displacement, Matrix.CreateRotationZ(angle));
            _vel = displacement * 2.0f;
        }
    }

}
