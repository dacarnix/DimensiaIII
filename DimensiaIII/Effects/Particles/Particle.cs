﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Effects.Particles
{
    /// <summary>
    /// The base class for a PARTICLE. 
    /// Extends effect, and thus is an effect also.
    /// 
    /// Defines the common methods used in particles, which are not used
    /// by effect also.
    /// 
    /// There will be potentially thousands of particles, so please make the types
    /// as small as possible (ie bytes/shorts over ints..)
    /// </summary>
    public abstract class Particle : Effect
    {

        #region Variables and Properties
        //Texture width/height
        protected int _width, _height;

        //how the velocity in the X,Y slows down over time
        //set this to ONE if you dont want any. < 1 slows down, > 1 speeds up (but why?!)
        protected float _frictionX, _frictionY;

        //Amount alpha changes over time, rotation is how much the texture is rotated.
        protected float _alphaDecay, _rotation, _rotationSpeed;

        //recorded as MILLISECONDS!
        //set this to INFINITE_LIFE if you want no life span
        protected int _lifeSpan;

        protected const int INFINITE_LIFE = 100000;

        #endregion

        #region Constructor


        #endregion

        #region XNA Methods

        public override bool Update(GameTime gameTime)
        {

            _pos += _vel;

            _vel.Y /= _frictionY;

            _vel.X /= _frictionX;

            //Rotate 2 PI radians
            _rotation = (_rotation) % MathHelper.TwoPi;

            _alpha -= _alphaDecay;

            if (_lifeSpan > 0 && INFINITE_LIFE != _lifeSpan)
            {
                _lifeSpan -= (short)gameTime.ElapsedGameTime.TotalMilliseconds;
            }

            //kill it
            bool alive = (_lifeSpan > 0 && _alpha > 0.1f);

            return alive;
        }

        public override void Draw(SpriteBatch batch) { }
        //Textures will be determined in sub-classes
        //Will be static to save on texture memory

        #endregion

        #region Method
                
        public virtual void Regenerate(Vector2 pos, Color c)
        {
            _color = c; //default values
            _orgPos = _pos = pos;
            _frictionX = _frictionY = 1.005f;
            _width = _height = 1;
            _lifeSpan = INFINITE_LIFE;
            _alpha = 1;
            MakeUnique();
        }

        #endregion
        
    }
}
