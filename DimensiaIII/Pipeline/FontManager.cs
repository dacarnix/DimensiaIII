﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Pipeline
{
    public static class FontManager
    {

        #region Variables and Properties

        private static SpriteFont _clean;

        public static SpriteFont Clean
        {
            get { return _clean;  }
        }

        #endregion

        #region XNA Methods

        public static void Initilize()
        {

        }

        public static void LoadContent(ContentManager content)
        {
            _clean = content.Load<SpriteFont>("Fonts\\Clean");
        }

        #endregion

        #region Methods
        #endregion

    }
}
