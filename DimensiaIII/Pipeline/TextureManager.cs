﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using DimensiaIII.WorldEngine.TileEngine;

namespace DimensiaIII.Pipeline
{
    public static class TextureManager
    {

        #region Variables and Properties

        private static Texture2D _pixel;
        private static Texture2D _rain;
        private static GraphicsDevice _device;

        public static GraphicsDevice Device
        {
            get { return _device; }
        }

        public static Texture2D Pixel
        {
            get { return _pixel;  }
        }

        public static Texture2D Rain
        {
            get { return _rain; }
        }

        #endregion

        #region XNA Methods

        public static void Initilize()
        {

        }

        public static void LoadContent(ContentManager content, GraphicsDevice graphicsDevice)
        {
           //_pixel = content.Load<Texture2D>("pixel");
            _pixel = new Texture2D(graphicsDevice, 1, 1);
            _pixel.SetData<Color>(new Color[] { Color.White });

            _rain = content.Load<Texture2D>("rain");
            _device = graphicsDevice;
            Engine.LoadTileSets(content); //load all the tile sets
        }

        #endregion

        #region Methods
        #endregion

    }
}
