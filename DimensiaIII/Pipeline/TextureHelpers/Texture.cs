﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace DimensiaIII.Pipeline.TextureHelpers
{
    /// <summary>
    /// Basically a wrapper to a texture, which holds support for animations/tilesets
    /// </summary>
    public class Texture
    {
        #region Variables and Properties
        private Texture2D _texture;
        //Preprocessed array rectangles per row
        //[ROWS][CELLS]
        private Rectangle[,] _destRects;

        private int _gridWidth, _gridHeight;
        private int _maxRectanglesAcross, _maxRectanglesDown;

        //does the texture hold multiple (ie tileset or animation), or is it single
        private bool _singleTexture;

        //Which dest rectangle are we at?
        //Can modify this via a method
        private Point _activeRectangle;

        public Texture2D MainTexture
        {
            get { return _texture; }
        }

        public int Width
        {
            get { return _gridWidth; }
        }

        public int Height
        {
            get { return _gridHeight; }
        }

        //The active rectangle we're viewing on the texture
        public Rectangle ViewRectangle
        {
           get
            {
                return _destRects[_activeRectangle.Y, _activeRectangle.X]; 
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Standard 1 texture per texture. 
        /// </summary>
        /// <param name="texture"></param>
        public Texture(Texture2D texture)
        {
            _gridWidth = texture.Width;
            _gridHeight = texture.Height;
            _destRects = new Rectangle[1, 1];
            _singleTexture = true;
            _maxRectanglesAcross = _maxRectanglesDown = 1;
            _destRects[0,0] = new Rectangle(0, 0, _gridWidth, _gridHeight);
            _activeRectangle = new Point(0, 0);
        }

        /// <summary>
        /// This constructor is made for the textures which contain animations etc. or multiple 'textures' per one
        /// </summary>
        /// <param name="tex"></param>
        /// <param name="gridWidth">How big each grid is</param>
        /// <param name="gridHeight"></param>
        /// <param name="xOffset">the gap between succesive elements in the X direction</param>
        /// <param name="yOffset">likewise but for vertical</param>
        public Texture(Texture2D tex, int gridWidth, int gridHeight, int xOffset, int yOffset)
        {
            _gridWidth = gridWidth; _gridHeight = gridHeight;
            int rectsWide = (int)Math.Ceiling(tex.Width / (float)((gridWidth + xOffset)));
            int rectsHeight = (int)Math.Ceiling(tex.Height / (float)((gridHeight + yOffset)));
            _destRects = new Rectangle[rectsHeight, rectsWide];
            for (int y = 0; y < rectsHeight; y++)
            {
                for (int x  = 0; x < rectsWide; x++)
                {
                    _destRects[y, x] = new Rectangle(x * (_gridWidth + xOffset), y * (_gridHeight + yOffset), _gridWidth, _gridHeight);
                }
            }
            _maxRectanglesAcross = rectsWide;
            _maxRectanglesDown = rectsHeight;
            _singleTexture = false;
            _activeRectangle = new Point(0, 0);
        }

        #endregion

        #region XNA Methods

        #endregion

        #region Animation Methods

        public void ChangeRectangle(int x, int y)
        {
            if (x >= _maxRectanglesAcross || y >= _maxRectanglesDown)
            {
                throw new IndexOutOfRangeException("Invalid co-ordinates for the given texture");
            }
            _activeRectangle = new Point(x, y);
        }

        #endregion

        #region Other

        #endregion

    }
}
