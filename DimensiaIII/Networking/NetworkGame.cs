﻿using System;
using Microsoft.Xna.Framework;


using DimensiaIII.Entities;

using DimensiaIII.Networking.Chat;

using Lidgren.Network;


namespace DimensiaIII.Networking
{
    //I think NetworkGame should be a wrapper for everything
    //Literally every component of the game requires some form of network communication
    //why not make this the base for the game logic?

    //This will encapsulate any areas which require some form of network communication
    //So far this is.. 
    //-players
    //-universe
    //-chat

    //there is NO 'draw methods', this purely handles updates (ie network packets)

    enum NetworkState
    {
        NotSet,
        Hosting,
        Joining,
        Ingame
    }

    public static class NetworkGame
    {
        #region Variables and Properties

        private static ChatLog _chatLog;
        private static ServerBrowser _serverBrowser;

        private static bool _isHost = false, _isClient = false, _isSinglePlayer = false; //These are for NETWORKING.
        private static int _port;

        //this returns true is isClient == isHost, ie invalid set up
        //this will always start true (as it should) as the player has yet to 
        //set up the networking gateway 
        public static bool IsNetworkGame { get { return !_isSinglePlayer; } }
        public static bool InvalidNetworkSetUp { get { return _isClient == _isHost; } }

        private static NetworkState _currentState = NetworkState.NotSet;
        private static NetPeerConfiguration _netConfig;
        private static NetPeer _user; //this can either be NetClient OR NetServer

        //private static INetEncryption _key;

        private static float _cUpdateTimer; // a timer to pulse updates at a given frequency (variable below)
        private static float BaseContinuousUpdateRate; //Set in 'setupnetworking' method

        public static ChatLog Chat { get { return _chatLog; } }
        public static ServerBrowser Browser { get { return _serverBrowser; } }

        public static int PeopleConnected
        {
            get
            {
                if (_user == null) return 0;

                return _user.ConnectionsCount;
            }
        }

        public static bool ConnectedToAGame
        {
            get 
            {
                return PeopleConnected > 0 || _isHost; //this is mainly for Client purposes, ie checking it has connected
            }
        }

        #endregion

        #region Set up

        public static void SetUpNetworking(bool isHost, bool singlePlayer, string ip, int port)
        {

            CleanUp();

            _port = port;
            _chatLog = new ChatLog();
            _serverBrowser = new ServerBrowser();
            BaseContinuousUpdateRate = (_isHost) ? 1000 / 30 : 1000 / 60; //30 fps for host?
            _cUpdateTimer = 0; //start at 0 and count up.

            _isHost = isHost;
            _isClient = (_isHost) ? false : true;

            _isSinglePlayer = singlePlayer;
            //these two can NOT be equal

            if (singlePlayer && isHost) throw new Exception("Invalid criteria"); //invalid 

            if (singlePlayer) return; //no need for any networking components.

            _netConfig = new NetPeerConfiguration(Main.GameName);

            _netConfig.MaximumConnections = EntityManager.MaxOnlinePlayers;

            if (_isHost)
            {
                _netConfig.Port = port; //not recommended for clientsd
                _netConfig.EnableUPnP = true; //attempt a port forward
                _netConfig.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);

                _user = new NetServer(_netConfig);
                _currentState = NetworkState.Hosting;
                _user.Start();
                _user.UPnP.ForwardPort(port, "Port forward attempt");
            }
            else
            {
                _netConfig.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);
                _user = new NetClient(_netConfig);
                _user.Start(); //start the networking componenet
            }

            //_key = new NetAESEncryption("cockmuncherchandy"); //for encryption/decryption

            if (!_isHost)
            {
                ((NetClient)_user).DiscoverLocalPeers(port); //attempt to find connections on the specificed port
                //list them on the ServerBrowser
            }

        }

        private static void CleanUp()
        {
            //drop any previous connections here etc (TO DO)

        }

        #endregion

        #region Update

        public static void Update(GameTime gameTime)
        {

            _chatLog.Update(gameTime);
            if (!_isHost) _serverBrowser.Update(gameTime, _user);

            if (_isSinglePlayer) return;

            NetIncomingMessage _message;
            while ((_message = _user.ReadMessage()) != null)
            {
                if (_isHost)
                {
                    HandleServerMessage(_message);
                }
                else
                {
                    HandleClientMessage(_message);
                }

                _user.Recycle(_message); //clean up

                if (_isHost) //was on example?
                {
                    System.Threading.Thread.Sleep(1);
                }
            }

            SendContinuousUpdates(gameTime); //player updates etc etc
        }

        #endregion

        #region Message Sending

        //These are updates which need to be sent frequently
        //player updates, particles, etc.
        private static void SendContinuousUpdates(GameTime gameTime)
        {
            //MAKE SURE YOU USE THE SECOND OVERLOAD, preallocate
            //http://code.google.com/p/lidgren-network-gen3/wiki/Optimization

            if (_user.ConnectionsCount > 0) //Someones actually connected to send crap to
            {

                _cUpdateTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

                if (_cUpdateTimer > BaseContinuousUpdateRate) //It's time to send updates!
                {
                    _cUpdateTimer = 0;
                    NetOutgoingMessage _toSend = _user.CreateMessage(); //to send to client/server
                    _toSend.Write((byte)NetworkMessageType.Player);
                    _toSend.Write((int)EntityManager.MainPlayer.Position.X);
                    _toSend.Write((int)EntityManager.MainPlayer.Position.Y);
                    _user.SendMessage(_toSend, _user.Connections[0], NetDeliveryMethod.UnreliableSequenced);
                }

            }

        }

        #endregion

        #region Message Receiving

        #region Hosting

        private static void HandleServerMessage(NetIncomingMessage message)
        {
            switch (message.MessageType)
            {
                case NetIncomingMessageType.DiscoveryRequest:

                    NetOutgoingMessage response = _user.CreateMessage();
                    response.Write("Andrews Server");
                    response.Write(DateTime.Now.Millisecond);
                    _user.SendDiscoveryResponse(response, message.SenderEndPoint);

                    break;
                case NetIncomingMessageType.StatusChanged:
                    if (message.SenderConnection.Status == NetConnectionStatus.Connected)
                    {
                        AddChatMessage("Client connected to your server!");

                        byte randNum = Rand.GetRandByte();

                        NetOutgoingMessage reply = _user.CreateMessage();
                        reply.Write((byte)NetworkMessageType.World);
                        reply.Write(randNum);
                        _user.SendMessage(reply, message.SenderConnection, NetDeliveryMethod.ReliableOrdered);

                        EntityManager.AddOnlinePlayer(message.SenderEndPoint.ToString());

                    }
                    else if (message.SenderConnection.Status == NetConnectionStatus.Disconnected)
                    {
                        AddChatMessage("Client disconnected from your server!");
                        _user.Connections.Remove(message.SenderConnection);
                    }
                    break;
                case NetIncomingMessageType.Data:
                    byte type = message.ReadByte();
                    if (type == (byte)NetworkMessageType.Player) //player
                    {
                        int x = message.ReadInt32();
                        int y = message.ReadInt32();
                        EntityManager.UpdateOnlinePosition(message.SenderEndPoint.ToString(), new Vector2(x, y));
                    }

                    break;
                default:
                    //System.Windows.Forms.MessageBox.Show("Unhandled type: " + message.MessageType);
                    break;
            }
        }

        #endregion

        #region Client side

        public static void AttemptConnection(System.Net.IPEndPoint ip)
        {
            _user.Connect(ip);
        }

        private static void HandleClientMessage(NetIncomingMessage message)
        {
            switch (message.MessageType)
            {
                case NetIncomingMessageType.DiscoveryResponse:
                    //_user.Connect(message.SenderEndPoint); //And Connect the Server with IP (string) and host (int) parameters
                    string name = message.ReadString();
                    int ping = DateTime.Now.Millisecond - message.ReadInt32();
                    _serverBrowser.AddServer(name, message.SenderEndPoint.ToString().Split(':')[0], int.Parse(message.SenderEndPoint.ToString().Split(':')[1]), ping);

                    break;
                case NetIncomingMessageType.StatusChanged:
                    if (message.SenderConnection.Status == NetConnectionStatus.Connected)
                    {
                        AddChatMessage("Connected to a server!");
                        EntityManager.AddOnlinePlayer(message.SenderEndPoint.ToString());
                    }
                    break;
                case NetIncomingMessageType.Data:


                    break;
                default:
                    //System.Windows.Forms.MessageBox.Show("Unhandled type: " + message.MessageType + " with " + System.Text.Encoding.UTF8.GetString(message.Data));
                    break;
            }
        }

        #endregion

        #region General Handling

        private static void HandleUpdates(GameTime gameTime)
        {
            //_message holds the current message
            
        }

        #endregion

        #endregion

        #region Chat

        public static void AddChatMessage(string message)
        {
            AddChatMessage(message, "Server"); //default 
        }

        public static void AddChatMessage(string message, string author)
        {
            ChatLine line = new ChatLine(message, author);
            _chatLog.AddMessage(line);
        }

        #endregion

        #region Methods

        #endregion

    }
}
