﻿namespace DimensiaIII.Networking
{
    //sexy, eh?
    //Limit this to EIGHT unique message types
    //this will keep the 'headers' efficient and not waste bits on traffic
    public enum NetworkMessageType : byte
    {

        Connect = 0,
        Disconnect = 1,
        Player = 2,
        NPC = 3,
        World = 4,
        Chat = 5,
        Ping = 6
    }


}
