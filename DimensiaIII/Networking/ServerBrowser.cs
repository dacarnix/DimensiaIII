﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Lidgren.Network;

using DimensiaIII.Networking.Browser;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Networking
{
    //This class essentially functions as a GUI for avaliable servers
    public class ServerBrowser
    {

        #region Variables and Properties

        List<ServerData> _servers;
        HashSet<string>_uniqueServers; //name explains it, ensures no duplicates, string is IP

        public List<ServerData> Data { get { return _servers; } }

        #endregion

        #region Constructor

        public ServerBrowser()
        {
            _servers = new List<ServerData>();
            _uniqueServers = new HashSet<string>();
        }

        #endregion

        #region Methods

        public void Refresh()
        {
            _servers.Clear();
            _uniqueServers.Clear();
        }

        public void AddServer(string name, string ip, int port, int ping)
        {
            if (!_uniqueServers.Contains(ip))
            {
                _uniqueServers.Add(ip);
                _servers.Add(new ServerData(name, ip, port, ping));
            }
        }

        #endregion

        #region XNA Methods

        public void Update(GameTime gameTime, NetPeer client)
        {
            Vector2 drawFrom = new Vector2(500, 300);

            foreach (ServerData sd in _servers)
            {
                Rectangle collision = MathAid.Vector2ToRectangle(drawFrom, Pipeline.FontManager.Clean.MeasureString(sd.Name + " with ping of " + sd.Ping + "ms"));

                drawFrom.Y += 100;
                if (Input.MouseCollision(collision) && Input.LeftClick)
                {
                    sd.HasClick = true;
                }
                else
                {
                    sd.HasClick = false;
                }
            }
        }

        public void Draw(SpriteBatch batch)
        {
            Vector2 drawFrom = new Vector2(500, 300);

            foreach (ServerData sd in _servers)
            {
                batch.DrawString(Pipeline.FontManager.Clean, sd.Name + " with ping of " + sd.Ping + "ms", drawFrom, Color.Red);
                drawFrom.Y += 100;
            }

        }

        #endregion

    }
}
