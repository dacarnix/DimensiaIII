﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace DimensiaIII.Networking.Chat
{
    public class ChatLog
    {

        #region Variables and Properties

        private List<ChatLine> _chat; //holds all the chats
        private bool _chatOpen; //whether or not typing is allowed
        private string _currentMessage; // this is for the user to type into (and send)

        private static Keys _chatKey; //this is the key which will toggle chat

        public static Keys ChatKey { get { return _chatKey; } set { _chatKey = value; } }

        #endregion

        #region Constructor

        public ChatLog()
        {
            _chat = new List<ChatLine>();
            _chatKey = Keys.Enter; //default value for now, change this later
            _chatOpen = false;
            _currentMessage = ""; //starts blank
        }

        #endregion

        #region XNA Methods

        public void Update(GameTime gameTime)
        {
            if (Input.KeyPressed(_chatKey)) ToggleChat();
            
            //oldest lines will be removed first, therefore loop through it backwards. (for effiency)
            for (int i = _chat.Count - 1; i >= 0; i--)
            {
                if (!_chat[i].Update(gameTime)) _chat.RemoveAt(i);
            }
        }

        public void Draw(SpriteBatch batch)
        {
            int c = 0;
            for (int i = _chat.Count - 1; i >= 0; i--)
            {
                batch.DrawString(Pipeline.FontManager.Clean, _chat[i].ToString(), new Vector2(15, Main.ScreenHeight - 32 - (c * 30)), Color.White * _chat[i].Alpha);
                c++;
            }
        }

        #endregion

        #region Methods

        private void ToggleChat()
        {
            _chatOpen = (_chatOpen) ? false : true; //toggle chat
        }

        public void AddMessage(ChatLine line)
        {
            _chat.Add(line);
        }

        #endregion     
        

    }
}
