﻿
using Microsoft.Xna.Framework;

namespace DimensiaIII.Networking.Chat
{
    //Stores a single 'chat' line, ChatLog will hold a list of this. 
    public class ChatLine
    {

        #region Variables and Properties

        private string _message, _author;
        private float _alpha, _timeAlive, _deprecation;

        private bool _forceDeath; //for error handling

        public float Alpha { get { return _alpha; } }

        #endregion

        #region Constructor

        public ChatLine(string message, string author)
        {
            _message = message; _author = author; //pretty straight forward
            _alpha = 1; _timeAlive = 0;
            _deprecation = 0.005f; //how fast a message dissapears.
            _forceDeath = false;
        }

        #endregion

        #region XNA Methods

        public bool Update(GameTime gameTime)
        {
            return ((_alpha -= _deprecation) >= 0) && !_forceDeath;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            if (_author == null) _author = "Server";

            if (_message == null)
            {
                _forceDeath = true; //get rid of that!
                return ""; //shouldn't exist.
            } 

            return _author + ": " + _message;
        }

        #endregion     
        

    }
}
