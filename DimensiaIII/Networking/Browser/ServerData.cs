﻿using System;

namespace DimensiaIII.Networking.Browser
{
    //data for each server
    //fairly basic class, no regions needed =)
    public class ServerData
    {
        //stores variables related to found servers, pretty self-explanatory
        private string _ip, _name;
        private int _port,_pingTotal, _pingUpdates, _pingUpdateLimit; //ping updates shows how many times ping should be calculated
        private bool _hasClick;

        public string IP { get { return _ip; } }
        public string IPEndPoint { get { return _ip + ":" + _port.ToString(); } }

        public int Port { get { return _port; } }
        public bool HasClick { get { return _hasClick; } set { _hasClick = value; } }
        public string Name { get { return _name; } }
        public int Ping { 
            get {
                return (int)_pingTotal; 
            } 
        }

        public bool CanUpdatePing { get { return _pingUpdates < _pingUpdateLimit; } }

        public ServerData(string name, string ip, int port, int ping)
        {
            _hasClick = false;
            _name = name;
            _port = port;
            _ip = ip;
            _pingTotal = Math.Abs(ping);
            _pingUpdateLimit = 5; //Update 5 times?
            //System.Windows.Forms.MessageBox.Show(_name);
        }

        //only CLIENTS (ie people wanting to join) will be wanting to find servers
        //therefore valid to use a netclient

        private void UpdatePing(int ping)
        {
            _pingTotal += ping; //here it should update the ping.
            _pingUpdates++;
        }

    }
}
